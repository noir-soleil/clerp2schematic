/* TODO:
 * Reordered columns from CLERP
 * Part orientation vertical or horizontal
 * Polar caps
 * Inductors
 */

// For compilers that support precompilation, includes "wx/wx.h".
#include <wx/wxprec.h>

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

// for all others, include the necessary headers (this file is usually all you
// need because it includes almost all "standard" wxWidgets headers)
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include <wx/clipbrd.h>
#include <wx/config.h>
#include <wx/filename.h>
#include <wx/filepicker.h>
#include <wx/dcbuffer.h>
#include <wx/tglbtn.h>

#ifndef wxHAS_IMAGES_IN_RESOURCES
    #include "../sample.xpm"
#endif

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>

#include <vector>
#include <map>
#include <list>
#include <utility>
#include <tuple>

#define posCENTER 0x80000000


#define USE_ASYNCHRONOUS_CLIPBOARD_REQUEST  0

static long pick_long(const void *buf, size_t *idx)
{
	const unsigned char * b = (const unsigned char *)buf;
	long res = b[*idx + 0];
	res |= b[*idx + 1] << 8;
	res |= b[*idx + 2] << 16;
	res |= b[*idx + 3] << 24;
	*idx += 4;
	return res;
}

static short pick_short(const void *buf, size_t *idx)
{
	const unsigned char * b = (const unsigned char *)buf;
	short res = b[*idx + 0];
	res |= b[*idx + 1] << 8;
	*idx += 2;
	return res;
}

static unsigned char * write_const(unsigned char *buf, const void *v, size_t len)
{
	memcpy(buf, v, len);
	buf += len;
	return buf;
}

static unsigned char * write_string(unsigned char *buf, const std::string & str)
{
	size_t len = str.length();
	*buf++ = (len >> 0) & 0xFF;
	*buf++ = (len >> 8) & 0xFF;
	memcpy(buf, str.c_str(), len);
	buf += len;
	*buf++ = 0;
	return buf;
}

static unsigned char * write_short(unsigned char *buf, const short v)
{
	*buf++ = (v >> 0) & 0xFF;
	*buf++ = (v >> 8) & 0xFF;
	return buf;
}

struct partPos {
	int x1, x2, x3, y1, y2, y3;
	partPos()
		: x1(-1), x2(-1), x3(-1), y1(-1), y2(-1), y3(-1)
	{};
	partPos(int x1_v, int y1_v, int x2_v, int y2_v, int x3_v, int y3_v)
		: x1(x1_v), x2(x2_v), x3(x3_v), y1(y1_v), y2(y2_v), y3(y3_v)
	{};
	bool is_valid() const { return (this->x2 > this->x1) && (this->y2 > this->y1) && (this->x2 > 0) && (this->y2 > 0) && (this->x1 >= 0) && (this->y1 >= 0); };
	bool operator!() const { return !this->is_valid(); };
	void draw(wxDC * dc) const
	{
		if (!this->is_valid()) return;
		if (!dc) return;
		dc->DrawLine(this->x1, this->y1, this->x1, this->y2);
		dc->DrawLine(this->x1, this->y1, this->x2, this->y1);
		dc->DrawLine(this->x2, this->y1, this->x2, this->y2);
		dc->DrawLine(this->x1, this->y2, this->x2, this->y2);
	};
	void read(const void *buf, size_t *idx)
	{
		this->y1 = pick_short(buf, idx);
		this->x1 = pick_short(buf, idx);
		this->y2 = pick_short(buf, idx);
		this->x2 = pick_short(buf, idx);
		this->x3 = pick_short(buf, idx);
		this->y3 = pick_short(buf, idx);
	};
	std::string str() const
	{
		if (!this->is_valid()) return "partPos: INVALID";
		std::string ret;
		ret.resize(100);
		ret.resize(snprintf(&ret[0], 100, "partPos: (%i, %i)->(%i, %i):(%i, %i)", this->x1, this->y1, this->x2, this->y2, this->x3, this->y3));
		return ret;
	};
	void center(int x, int y)
	{
		if (!this->is_valid()) return;
		int w = this->x2 - this->x1;
		int h = this->y2 - this->y1;
		int dx = this->x1 - (x - w / 2);
		int dy = this->y1 - (y - h / 2);
		this->x1 -= dx;
		this->y1 -= dy;
		this->x2 -= dx;
		this->y2 -= dy;
		this->x3 -= dx;
		this->y3 -= dy;
	};
};

struct partBB {
	int w, h;
	partBB()
		: w(-1), h(-1)
	{};
	partBB(int w_v, int h_v)
		: w(w_v), h(h_v)
	{};
	bool is_valid() const { return (this->w > 0) && (this->h > 0); };
	bool operator!() const { return !this->is_valid(); };
	void draw(wxDC * dc, const struct partPos &pp, const int rot) const
	{
		if (!this->is_valid()) return;
		if (!dc) return;
		if (!pp.is_valid()) return;

		if ((rot & 1) == 0) {
			dc->DrawLine(pp.x3, pp.y3, pp.x3 + this->w, pp.y3);
			dc->DrawLine(pp.x3, pp.y3 + this->h, pp.x3 + this->w, pp.y3 + this->h);
			dc->DrawLine(pp.x3, pp.y3, pp.x3, pp.y3 + this->h);
			dc->DrawLine(pp.x3 + this->w, pp.y3, pp.x3 + this->w, pp.y3 + this->h);
		} else {
			dc->DrawLine(pp.x3, pp.y3, pp.x3 + this->h, pp.y3);
			dc->DrawLine(pp.x3, pp.y3 + this->w, pp.x3 + this->h, pp.y3 + this->w);
			dc->DrawLine(pp.x3, pp.y3, pp.x3, pp.y3 + this->w);
			dc->DrawLine(pp.x3 + this->h, pp.y3, pp.x3 + this->h, pp.y3 + this->w);
		}
	};
	void read(const void *buf, size_t *idx)
	{
		this->w = pick_short(buf, idx);
		this->h = pick_short(buf, idx);
	};
	std::string str() const
	{
		if (!this->is_valid()) return "partBB: INVALID";
		std::string ret;
		ret.resize(100);
		ret.resize(snprintf(&ret[0], 100, "partBB: %i x %i", this->w, this->h));
		return ret;
	};
};

struct partText {
	int x, y;
	std::map<int, int> xx, yy;
	int cx, cy;
	std::string name, value;
	int display;
	int rot;
	int flag;
	int color;
	wxFont font;
	partText()
		: x(0), y(0), cx(0), cy(0), display(0), rot(0), flag(0), color(0x30)
		  , font(7, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, "Arial")
	{};
	partText(int x_v, int y_v, const std::string &name_v, const std::string &value_v, int disp_v, int rot_v)
		: x(x_v), y(y_v), cx(x_v), cy(y_v), name(name_v), value(value_v), display(disp_v), rot(rot_v), flag(0), color(0x30)
		  , font(7, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, "Arial")
	{};
	partText(const std::map<int, int> &xx_v, const std::map<int, int> &yy_v, const std::string &name_v, const std::string &value_v, int disp_v, int rot_v)
		: x(xx_v.at(0)), y(yy_v.at(0)), xx(xx_v), yy(yy_v), cx(xx_v.at(0)), cy(yy_v.at(0)), name(name_v), value(value_v), display(disp_v), rot(rot_v), flag(0), color(0x30)
		  , font(7, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, "Arial")
	{};
	bool is_valid() const { return this->name.length() > 0; };
	bool operator!() const { return !this->is_valid(); };
	void draw(wxDC * dc, const struct partPos &pp, const struct partBB &bb, const int rot)
	{
		if (!this->is_valid()) return;
		if (!dc) return;
		if (!pp.is_valid()) return;

		if (this->display == 1 && this->value.length()) {
			dc->SetFont(this->font);
			wxColour t_colour(0x00, 0x00, 0x80, wxALPHA_OPAQUE);
			dc->SetTextForeground(t_colour);
			this->cx = this->xx.count(rot) ? this->xx[rot] : (this->xx.count(rot & 1) ? this->xx[rot & 1] : this->x);
			if (this->cx == posCENTER) {
				int c = ((rot & 1) ? bb.h / 2 : bb.w / 2);
				int tw, th;
				dc->GetTextExtent(this->value, &tw, &th);
				this->cx = c - tw / 2;
			}
			this->cy = this->yy.count(rot) ? this->yy[rot] : (this->yy.count(rot & 1) ? this->yy[rot & 1] : this->y);
			if (this->cy == posCENTER) {
				int c = ((rot & 1) ? bb.w / 2 : bb.h / 2);
				int tw, th;
				dc->GetTextExtent(this->value, &tw, &th);
				this->cy = c - th / 2;
			}
			dc->DrawText(wxString::Format("%s", this->value), this->cx + pp.x3, this->cy + pp.y3);
		}
	};
	unsigned char * writeDescriptor(unsigned char *buf) const
	{
		buf = write_const(buf, "\x27\xff\xe4\x5c\x39\x00\x00\x00\x00\xff\x7f\x03\x00\x02\x00", 15);
		buf = write_string(buf, this->name);
		buf = write_short(buf, this->cx);
		buf = write_short(buf, this->cy);
		*buf++ = this->flag & 0xff;
		*buf++ = this->rot & 0xff;
		*buf++ = this->color & 0xff;
		*buf++ = 0;
		*buf++ = this->display & 0xff;
		*buf++ = 0;
		return buf;
	};
};

enum graphicType {
	gNONE = 0,
	gLINE,
	gPIN,
};

struct partGraphic {
	enum graphicType type;
	std::vector<int> coords;
	std::string text;
	partGraphic() : type(gNONE) {};
	partGraphic(enum graphicType t, std::vector<int> cs) : type(t), coords(cs) {};
	partGraphic(enum graphicType t, std::vector<int> cs, std::string txt) : type(t), coords(cs), text(txt) {};
	bool is_valid() const { return this->type != gNONE; };
	bool operator!() const { return !this->is_valid(); };
	void draw(wxDC * dc, const struct partPos &pp, const struct partBB &bb, const int rot) const
	{
		if (!this->is_valid()) return;
		if (!dc) return;
		if (!pp.is_valid()) return;

		wxColour g_colour(0x00, 0x00, 0xFF, wxALPHA_OPAQUE);
		wxColour p_colour(0xFF, 0x00, 0x00, wxALPHA_OPAQUE);
		if (this->type == gPIN) {
			dc->SetPen(wxPen(p_colour, 1, wxPENSTYLE_SOLID));
		} else {
			dc->SetPen(wxPen(g_colour, 1, wxPENSTYLE_SOLID));
		}

		if (type == gLINE || type == gPIN) {
			if (rot == 0) {
				dc->DrawLine(pp.x3 + coords.at(0), pp.y3 + coords.at(1), pp.x3 + coords.at(2), pp.y3 + coords.at(3));
			} else if (rot == 1) {
				dc->DrawLine(pp.x3 + bb.h - coords.at(1), pp.y3 + coords.at(0), pp.x3 + bb.h - coords.at(3), pp.y3 + coords.at(2));
			}
		}
	};
};

static std::vector<std::string> tokenize(const std::string & str, char sep)
{
	std::vector<std::string> vec;
	std::string::size_type pos = 0, fpos;
	while ((fpos = str.find(sep, pos)) != std::string::npos)
	{
		vec.push_back(str.substr(pos, fpos - pos));
		pos = fpos + 1;
	}
	vec.push_back(str.substr(pos, std::string::npos));
	return vec;
}

static std::map<std::string, std::string> match_re(pcre2_code * re, pcre2_match_data * md, const std::string & subj)
{
	std::map<std::string, std::string> map;
	PCRE2_SPTR subject = (PCRE2_SPTR)subj.c_str();
	PCRE2_SIZE subject_length = subj.length();

	int rc = pcre2_match(
		re,                   /* the compiled pattern */
		subject,              /* the subject string */
		subject_length,       /* the length of the subject */
		0,                    /* start at offset 0 in the subject */
		0,                    /* default options */
		md,                   /* block for storing the result */
		NULL);                /* use default match context */

	if (rc <= 0) return map;

	PCRE2_SIZE* ovector = pcre2_get_ovector_pointer(md);

	PCRE2_SPTR name_table;
	uint32_t namecount;
	uint32_t name_entry_size;
	(void)pcre2_pattern_info(re, PCRE2_INFO_NAMECOUNT, &namecount);

	if (namecount != 0) {
		PCRE2_SPTR tabptr;

		/* Before we can access the substrings, we must extract the table for
		   translating names to numbers, and the size of each entry in the table. */

		(void)pcre2_pattern_info(re, PCRE2_INFO_NAMETABLE, &name_table);

		(void)pcre2_pattern_info(re, PCRE2_INFO_NAMEENTRYSIZE, &name_entry_size);

		/* Now we can scan the table and, for each entry, print the number, the name,
		   and the substring itself. In the 8-bit library the number is held in two
		   bytes, most significant first. */

		tabptr = name_table;
		for (int i = 0; i < namecount; i++)
		{
			int j = (tabptr[0] << 8) | tabptr[1];
			std::string n{(const char *)(tabptr + 2), name_entry_size - 3};
			n.resize(strlen(n.c_str()));
			std::string v{(const char *)(subject + ovector[2*j]), ovector[2*j+1] - ovector[2*j]};
			map[n] = v;
			tabptr += name_entry_size;
		}
	}
	return map;
}

// Released into the public domain
template <typename T>
class ClipboardWatcher : public T
{
public:
    ClipboardWatcher() :
        m_hClipboardChainNext(NULL) {}
    template <typename A1>
    ClipboardWatcher(A1 a1) : T(a1),
        m_hClipboardChainNext(NULL) {}
    template <typename A1, typename A2>
    ClipboardWatcher(A1 a1, A2 a2) : T(a1, a2),
        m_hClipboardChainNext(NULL) {}
    template <typename A1, typename A2, typename A3>
    ClipboardWatcher(A1 a1, A2 a2, A3 a3) : T(a1, a2, a3),
        m_hClipboardChainNext(NULL) {}
    
    virtual WXLRESULT MSWWindowProc(WXUINT nMsg, WXWPARAM wParam, WXLPARAM lParam)
    {
        switch(nMsg)
        {
        case WM_CHANGECBCHAIN:
            if((HWND)wParam == m_hClipboardChainNext)
                m_hClipboardChainNext = (HWND)lParam;
            else if(m_hClipboardChainNext != NULL)
                SendMessage(m_hClipboardChainNext, nMsg, wParam, lParam);
            return 0;

        case WM_DRAWCLIPBOARD:
            OnClipboardChange();
            if(m_hClipboardChainNext != NULL)
                SendMessage(m_hClipboardChainNext, nMsg, wParam, lParam);
            return 0;

        case WM_DESTROY:
            if(m_hClipboardChainNext)
                ChangeClipboardChain((HWND)this->GetHandle(), m_hClipboardChainNext);
            break;
        }
        return T::MSWWindowProc(nMsg, wParam, lParam);
    }

protected:
    void BeginWatchingClipboard()
    {
        m_hClipboardChainNext = SetClipboardViewer((HWND)this->GetHandle());
    }

    virtual void OnClipboardChange() = 0;

private:
    HWND m_hClipboardChainNext;
};

class ConfigPanel : public wxPanel
{
	public:
		ConfigPanel(wxWindow *parent, int id = wxID_ANY);
		~ConfigPanel() {};

	private:
		enum ids_ {
			idFln_CompLib = 1,
			idTxt_Separator,

		} ids_;
};

class ConfigFrame : public wxFrame
{
	public:
		ConfigFrame(wxWindow * parent);
		~ConfigFrame() {};

	private:
		ConfigPanel *mp_configPanel;
};


static unsigned char buffer[65536];

enum entryType {
	eNONE = 0,
	eCONST,
	eLIB_PATH,
	eREF_DESCR,
	eVAL_DESCR,
	eREF_VALUE,
	eVAL_VALUE,
	eFIELDS,
	eROTATION,
};

struct entry {
	enum entryType t;
	const unsigned char *d;
	size_t s;
};

struct partDescriptor {
	std::string type;
	struct partText REF, VAL;
	struct partPos pos;
	struct partBB bb;
	int rot;
	std::map<std::string, std::string> fields;
	std::string lib;
	std::list<struct entry> entries;
	std::list<struct partGraphic> graphics;
	std::map<std::string, std::string> vfields;
};

static const unsigned char resistor[] = {
/* 0x0000: */ 0x0d,

/* 0x0001: */ 0x36,    0, 0x43, 0x3a, 0x5c, 0x43, 0x41, 0x44, 0x45, 0x4e, 0x43, 0x45, 0x5c, 0x53, 0x50, /* "6.C:\CADENCE\SP" */
/* 0x0010: */ 0x42, 0x5f, 0x31, 0x36, 0x2e, 0x35, 0x5c, 0x54, 0x4f, 0x4f, 0x4c, 0x53, 0x5c, 0x43, 0x41, 0x50, /* "B_16.5\TOOLS\CAP" */
/* 0x0020: */ 0x54, 0x55, 0x52, 0x45, 0x5c, 0x4c, 0x49, 0x42, 0x52, 0x41, 0x52, 0x59, 0x5c, 0x44, 0x49, 0x53, /* "TURE\LIBRARY\DIS" */
/* 0x0030: */ 0x43, 0x52, 0x45, 0x54, 0x45, 0x2e, 0x4f, 0x4c, 0x42,    0, /* "CRETE.OLB......\" */

/* 0x003a: */ 0x18,    0,    0, 0xff, 0xe4, 0x5c,
/* 0x0040: */ 0x39,    0,    0,    0,    0, 0x0f,    0, 0x52, 0x45, 0x53, 0x49, 0x53, 0x54, 0x4f, 0x52, 0x2e, /* "9......RESISTOR." */
/* 0x0050: */ 0x4e, 0x6f, 0x72, 0x6d, 0x61, 0x6c,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x30,    0,    0, /* "Normal.......0.." */
/* 0x0060: */    0, 0x06,    0, 0x29, 0x0a,    0,    0,    0,    0,    0,    0,    0, 0x0d,    0,    0,    0, /* "...)............" */
/* 0x0070: */ 0x03,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x29, 0x0d,    0,    0, /* "............)..." */
/* 0x0080: */    0, 0x03,    0,    0,    0, 0x07,    0,    0,    0, 0x09,    0,    0,    0,    0,    0,    0, /* "................" */
/* 0x0090: */    0,    0,    0,    0,    0, 0x29, 0x07,    0,    0,    0, 0x09,    0,    0,    0, 0x0d,    0, /* ".....).........." */
/* 0x00a0: */    0,    0, 0x0f,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x29, 0x0d, /* "..............)." */
/* 0x00b0: */    0,    0,    0, 0x0f,    0,    0,    0, 0x07,    0,    0,    0, 0x15,    0,    0,    0,    0, /* "................" */
/* 0x00c0: */    0,    0,    0,    0,    0,    0,    0, 0x29, 0x07,    0,    0,    0, 0x15,    0,    0,    0, /* ".......)........" */
/* 0x00d0: */ 0x0d,    0,    0,    0, 0x1b,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, /* "................" */
/* 0x00e0: */ 0x29, 0x0d,    0,    0,    0, 0x1b,    0,    0,    0, 0x0a,    0,    0,    0, 0x1e,    0,    0, /* ")..............." */
/* 0x00f0: */    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x14,    0, 0x1e, /* "................" */
/* 0x0100: */    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x02,    0, 0x1a,    0,    0, 0xff, 0xe4, 0x5c, 0x39, /* "..............\9" */
/* 0x0110: */    0,    0,    0,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x01,    0, 0x31,    0, 0x0a,    0, /* "............1..." */
/* 0x0120: */    0,    0,    0,    0,    0,    0, 0x0a,    0,    0,    0, 0xf6, 0xff, 0xff, 0xff, 0x20,    0, /* "................" */
/* 0x0130: */    0,    0, 0x04,    0,    0,    0, 0x1a, 0x01,    0,    0,    0,    0, 0x1a,    0,    0, 0xff, /* "................" */
/* 0x0140: */ 0xe4, 0x5c, 0x39,    0,    0,    0,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x01,    0, 0x32, /* ".\9............2" */
/* 0x0150: */    0, 0x0a,    0,    0,    0, 0x1e,    0,    0,    0, 0x0a,    0,    0,    0, 0x28,    0,    0, /* ".............(.." */
/* 0x0160: */    0, 0x20,    0, 0xe9, 0x0d, 0x04,    0,    0,    0, 0x1a,    0,    0,    0,    0,    0, 0x02, /* "................" */
/* 0x0170: */    0, 0x27, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, /* ".'..\9.........." */
/* 0x0180: */ 0x0e,    0, 0x50, 0x61, 0x72, 0x74, 0x20, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65, /* "..Part.Reference" */
/* 0x0190: */    0, 0x14,    0,    0,    0, 0x04,    0, 0x30, 0xc6, 0x01,    0, 0xf7, 0xff, 0xff, 0xff, 0x05, /* ".......0........" */
/* 0x01a0: */    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x90, 0x01,    0,    0,    0, /* "................" */
/* 0x01b0: */    0,    0,    0, 0x03, 0x02, 0x01, 0x22, 0x41, 0x72, 0x69, 0x61, 0x6c,    0, 0x43, 0x38, 0x32, /* "......"Arial.C82" */
/* 0x01c0: */ 0x7d,    0,    0, 0x50,    0,    0,    0, 0x91,    0,    0,    0, 0x90, 0x22, 0x08, 0x02, 0xe0, /* "}..P........"..." */
/* 0x01d0: */ 0x35, 0x08, 0x02,    0, 0x23, 0x08, 0x02, 0x27, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0, /* "5...#..'..\9...." */
/* 0x01e0: */ 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x05,    0, 0x56, 0x61, 0x6c, 0x75, 0x65,    0, 0x14,    0, /* "........Value..." */
/* 0x01f0: */ 0x0a,    0, 0x04,    0, 0x30,    0, 0x01,    0, 0xf7, 0xff, 0xff, 0xff, 0x05,    0,    0,    0, /* "....0..........." */
/* 0x0200: */    0,    0,    0,    0,    0,    0,    0,    0, 0x90, 0x01,    0,    0,    0,    0,    0,    0, /* "................" */
/* 0x0210: */ 0x03, 0x02, 0x01, 0x22, 0x41, 0x72, 0x69, 0x61, 0x6c,    0, 0x43, 0x38, 0x32, 0x7d,    0,    0, /* "..."Arial.C82}.." */
/* 0x0220: */ 0x50,    0,    0,    0, 0x91,    0,    0,    0, 0x90, 0x22, 0x08, 0x02, 0xe0, 0x35, 0x08, 0x02, /* "P........"...5.." */
/* 0x0230: */    0, 0x23, 0x08, 0x02,    0,    0,    0,    0,    0,    0, 0x01,    0, 0x52,    0,    0,    0, /* ".#..........R..." */
/* 0x0240: */    0, 0x06,    0,

/* 0x0243: */ 0x36,    0, 0x43, 0x3a, 0x5c, 0x43, 0x41, 0x44, 0x45, 0x4e, 0x43, 0x45, 0x5c, /* "6.C:\CADENCE\" */
/* 0x0250: */ 0x53, 0x50, 0x42, 0x5f, 0x31, 0x36, 0x2e, 0x35, 0x5c, 0x54, 0x4f, 0x4f, 0x4c, 0x53, 0x5c, 0x43, /* "SPB_16.5\TOOLS\C" */
/* 0x0260: */ 0x41, 0x50, 0x54, 0x55, 0x52, 0x45, 0x5c, 0x4c, 0x49, 0x42, 0x52, 0x41, 0x52, 0x59, 0x5c, 0x44, /* "APTURE\LIBRARY\D" */
/* 0x0270: */ 0x49, 0x53, 0x43, 0x52, 0x45, 0x54, 0x45, 0x2e, 0x4f, 0x4c, 0x42,    0, /* "ISCRETE.OLB....\" */

/* 0x027c: */ 0x06, 0xff, 0xe4, 0x5c,
/* 0x0280: */ 0x39,    0,    0,    0,    0, 0x08,    0, 0x52, 0x45, 0x53, 0x49, 0x53, 0x54, 0x4f, 0x52,    0, /* "9......RESISTOR." */
/* 0x0290: */ 0x08,    0, 0x52, 0x45, 0x53, 0x49, 0x53, 0x54, 0x4f, 0x52,    0, 0x01,    0, 0x0f,    0, 0x52, /* "..RESISTOR.....R" */
/* 0x02a0: */ 0x45, 0x53, 0x49, 0x53, 0x54, 0x4f, 0x52, 0x2e, 0x4e, 0x6f, 0x72, 0x6d, 0x61, 0x6c,    0, /* "ESISTOR.Normal." */

/* 0x02af: */ 0x36,
/* 0x02b0: */    0, 0x43, 0x3a, 0x5c, 0x43, 0x41, 0x44, 0x45, 0x4e, 0x43, 0x45, 0x5c, 0x53, 0x50, 0x42, 0x5f, /* ".C:\CADENCE\SPB_" */
/* 0x02c0: */ 0x31, 0x36, 0x2e, 0x35, 0x5c, 0x54, 0x4f, 0x4f, 0x4c, 0x53, 0x5c, 0x43, 0x41, 0x50, 0x54, 0x55, /* "16.5\TOOLS\CAPTU" */
/* 0x02d0: */ 0x52, 0x45, 0x5c, 0x4c, 0x49, 0x42, 0x52, 0x41, 0x52, 0x59, 0x5c, 0x44, 0x49, 0x53, 0x43, 0x52, /* "RE\LIBRARY\DISCR" */
/* 0x02e0: */ 0x45, 0x54, 0x45, 0x2e, 0x4f, 0x4c, 0x42,    0, /* "ETE.OLB......\9." */

/* 0x02e8: */ 0x1f,    0,    0, 0xff, 0xe4, 0x5c, 0x39,    0,
/* 0x02f0: */    0,    0,    0, 0x08,    0, 0x52, 0x45, 0x53, 0x49, 0x53, 0x54, 0x4f, 0x52,    0, 0x01,    0, /* ".....RESISTOR..." */
/* 0x0300: */ 0x52,    0,    0,    0,    0,    0,    0,    0, 0x01,    0, 0x20, 0xff, 0xe4, 0x5c, 0x39,    0, /* "R............\9." */
/* 0x0310: */    0,    0,    0,    0,    0,    0, 0x08,    0, 0x52, 0x45, 0x53, 0x49, 0x53, 0x54, 0x4f, 0x52, /* "........RESISTOR" */
/* 0x0320: */    0, 0x02,    0, 0x01,    0, 0x31,    0, 0x7f, 0x01,    0, 0x32,    0, 0x7f, 0x0d,
/* 0x032E: */	0x02,    0,
/* 0x0330: */ 0x03,    0, 0x50, 0x2f, 0x4e,    0, /* "..P/N." */

/* 0x0336: */ 0x0a,    0, 0x31, 0x36, 0x31, 0x43, 0x31, 0x30, 0x30, 0x30, /* "..161C1000" */
/* 0x0340: */ 0x31, 0x53,    0,

/* 0x0343: */ 0x0d,    0, 0x50, 0x43, 0x42, 0x20, 0x46, 0x6f, 0x6f, 0x74, 0x70, 0x72, 0x69, /* "1S...PCB.Footpri" */
/* 0x0350: */ 0x6e, 0x74,    0,

/* 0x0353: */ 0x04,    0, 0x30, 0x34, 0x30, 0x32,    0,

/* 0x035a: */ 0xff, 0xe4, 0x5c, 0x39,    0,    0, /* "nt...0402...\9.." */
/* 0x0360: */    0,    0, 0x08,    0, 0x49, 0x4e, 0x53, 0x30, 0x30, 0x30, 0x30, 0x30,    0, /* "....INS00000.6.C" */

/* 0x036d: */ 0x36,    0, 0x43,
/* 0x0370: */ 0x3a, 0x5c, 0x43, 0x41, 0x44, 0x45, 0x4e, 0x43, 0x45, 0x5c, 0x53, 0x50, 0x42, 0x5f, 0x31, 0x36, /* ":\CADENCE\SPB_16" */
/* 0x0380: */ 0x2e, 0x35, 0x5c, 0x54, 0x4f, 0x4f, 0x4c, 0x53, 0x5c, 0x43, 0x41, 0x50, 0x54, 0x55, 0x52, 0x45, /* ".5\TOOLS\CAPTURE" */
/* 0x0390: */ 0x5c, 0x4c, 0x49, 0x42, 0x52, 0x41, 0x52, 0x59, 0x5c, 0x44, 0x49, 0x53, 0x43, 0x52, 0x45, 0x54, /* "\LIBRARY\DISCRET" */
/* 0x03a0: */ 0x45, 0x2e, 0x4f, 0x4c, 0x42,    0,

/* 0x03a6: */ 0x0f,    0, 0x52, 0x45, 0x53, 0x49, 0x53, 0x54, 0x4f, 0x52, /* "E.OLB...RESISTOR" */
/* 0x03b0: */ 0x2e, 0x4e, 0x6f, 0x72, 0x6d, 0x61, 0x6c,    0, 0x15, 0xaf,    0,    0, 0x0d, 0x02, 0xa3, 0x01, /* ".Normal........." */
/* 0x03c0: */ 0x49, 0x02, 0xc7, 0x01, 0xa4, 0x01, 0x1c, 0x02, 0x30,

	/* ROT */
/* 0x03c9: */	0,

/* 0x03ca: */	0x0d,    0, 0x02,    0,

/* 0x03ce: */	0x27, 0xff, /* "I.......0.....'." */
/* 0x03d0: */ 0xe4, 0x5c, 0x39,    0,    0,    0,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x0e,    0, 0x50, /* ".\9............P" */
/* 0x03e0: */ 0x61, 0x72, 0x74, 0x20, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65,    0, 0x11,    0, /* "art.Reference..." */
/* 0x03f0: */ 0x03,    0,    0,    0, 0x30,    0, 0x01,    0, 0x27, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0, /* "....0...'..\9..." */
/* 0x0400: */    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x05,    0, 0x56, 0x61, 0x6c, 0x75, 0x65,    0, 0x11, /* ".........Value.." */
/* 0x0410: */    0, 0x0f,    0,    0,    0, 0x30, 0x45, 0x01,    0,

/* 0x0419: */	0x18,

/* 0x041a: */ 0x36,    0, 0x43, 0x3a, 0x5c, 0x43, /* ".....0E...6.C:\C" */
/* 0x0420: */ 0x41, 0x44, 0x45, 0x4e, 0x43, 0x45, 0x5c, 0x53, 0x50, 0x42, 0x5f, 0x31, 0x36, 0x2e, 0x35, 0x5c, /* "ADENCE\SPB_16.5\" */
/* 0x0430: */ 0x54, 0x4f, 0x4f, 0x4c, 0x53, 0x5c, 0x43, 0x41, 0x50, 0x54, 0x55, 0x52, 0x45, 0x5c, 0x4c, 0x49, /* "TOOLS\CAPTURE\LI" */
/* 0x0440: */ 0x42, 0x52, 0x41, 0x52, 0x59, 0x5c, 0x44, 0x49, 0x53, 0x43, 0x52, 0x45, 0x54, 0x45, 0x2e, 0x4f, /* "BRARY\DISCRETE.O" */
/* 0x0450: */ 0x4c, 0x42,    0,

/* 0x0453: */ 0x18,    0,    0, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0, 0x0f,    0, /* "LB......\9......" */
/* 0x0460: */ 0x52, 0x45, 0x53, 0x49, 0x53, 0x54, 0x4f, 0x52, 0x2e, 0x4e, 0x6f, 0x72, 0x6d, 0x61, 0x6c,    0, /* "RESISTOR.Normal." */
/* 0x0470: */ 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x30,    0,    0,    0, 0x06,    0, 0x29, 0x0a,    0,    0, /* "......0.....)..." */
/* 0x0480: */    0,    0,    0,    0,    0, 0x0d,    0,    0,    0, 0x03,    0,    0,    0,    0,    0,    0, /* "................" */
/* 0x0490: */    0,    0,    0,    0,    0, 0x29, 0x0d,    0,    0,    0, 0x03,    0,    0,    0, 0x07,    0, /* ".....).........." */
/* 0x04a0: */    0,    0, 0x09,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x29, 0x07, /* "..............)." */
/* 0x04b0: */    0,    0,    0, 0x09,    0,    0,    0, 0x0d,    0,    0,    0, 0x0f,    0,    0,    0,    0, /* "................" */
/* 0x04c0: */    0,    0,    0,    0,    0,    0,    0, 0x29, 0x0d,    0,    0,    0, 0x0f,    0,    0,    0, /* ".......)........" */
/* 0x04d0: */ 0x07,    0,    0,    0, 0x15,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, /* "................" */
/* 0x04e0: */ 0x29, 0x07,    0,    0,    0, 0x15,    0,    0,    0, 0x0d,    0,    0,    0, 0x1b,    0,    0, /* ")..............." */
/* 0x04f0: */    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x29, 0x0d,    0,    0,    0, 0x1b,    0, /* ".........)......" */
/* 0x0500: */    0,    0, 0x0a,    0,    0,    0, 0x1e,    0,    0,    0,    0,    0,    0,    0,    0,    0, /* "................" */
/* 0x0510: */    0,    0,    0,    0,    0,    0, 0x14,    0, 0x1e,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, /* "................" */
/* 0x0520: */ 0x02,    0, 0x1a,    0,    0, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0, 0xff, 0x7f, 0x03, /* ".......\9......." */
/* 0x0530: */    0, 0x02,    0, 0x01,    0, 0x31,    0, 0x0a,    0,    0,    0,    0,    0,    0,    0, 0x0a, /* ".....1.........." */
/* 0x0540: */    0,    0,    0, 0xf6, 0xff, 0xff, 0xff, 0x20,    0,    0,    0, 0x04,    0,    0,    0, 0x1a, /* "................" */
/* 0x0550: */ 0x01,    0,    0,    0,    0, 0x1a,    0,    0, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0, /* "..........\9...." */
/* 0x0560: */ 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x01,    0, 0x32,    0, 0x0a,    0,    0,    0, 0x1e,    0, /* "........2......." */
/* 0x0570: */    0,    0, 0x0a,    0,    0,    0, 0x28,    0,    0,    0, 0x20,    0, 0xe9, 0x0d, 0x04,    0, /* "......(........." */
/* 0x0580: */    0,    0, 0x1a,    0,    0,    0,    0,    0, 0x02,    0, 0x27, 0xff, 0xe4, 0x5c, 0x39,    0, /* "..........'..\9." */
/* 0x0590: */    0,    0,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x0e,    0, 0x50, 0x61, 0x72, 0x74, 0x20, /* "...........Part." */
/* 0x05a0: */ 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65,    0, 0x14,    0,    0,    0, 0x04,    0, /* "Reference......." */
/* 0x05b0: */ 0x30, 0xc6, 0x01,    0, 0xf7, 0xff, 0xff, 0xff, 0x05,    0,    0,    0,    0,    0,    0,    0, /* "0..............." */
/* 0x05c0: */    0,    0,    0,    0, 0x90, 0x01,    0,    0,    0,    0,    0,    0, 0x03, 0x02, 0x01, 0x22, /* "..............."" */
/* 0x05d0: */ 0x41, 0x72, 0x69, 0x61, 0x6c,    0, 0x43, 0x38, 0x32, 0x7d,    0,    0, 0x50,    0,    0,    0, /* "Arial.C82}..P..." */
/* 0x05e0: */ 0x91,    0,    0,    0, 0x90, 0x22, 0x08, 0x02, 0xe0, 0x35, 0x08, 0x02,    0, 0x23, 0x08, 0x02, /* "....."...5...#.." */
/* 0x05f0: */ 0x27, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x05, /* "'..\9..........." */
/* 0x0600: */    0, 0x56, 0x61, 0x6c, 0x75, 0x65,    0, 0x14,    0, 0x0a,    0, 0x04,    0, 0x30,    0, 0x01, /* ".Value.......0.." */
/* 0x0610: */    0, 0xf7, 0xff, 0xff, 0xff, 0x05,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, /* "................" */
/* 0x0620: */    0, 0x90, 0x01,    0,    0,    0,    0,    0,    0, 0x03, 0x02, 0x01, 0x22, 0x41, 0x72, 0x69, /* "............"Ari" */
/* 0x0630: */ 0x61, 0x6c,    0, 0x43, 0x38, 0x32, 0x7d,    0,    0, 0x50,    0,    0,    0, 0x91,    0,    0, /* "al.C82}..P......" */
/* 0x0640: */    0, 0x90, 0x22, 0x08, 0x02, 0xe0, 0x35, 0x08, 0x02,    0, 0x23, 0x08, 0x02,    0,    0,    0, /* ".."...5...#....." */
/* 0x0650: */    0,    0,    0, 0x01,    0, 0x52,    0,    0,    0,    0, 0x06,    0,
/* 0x065C: */ /* <-- REF_VALUE */	0x02,    0, 0x52, 0x31, /* ".....R........R1" */
/* 0x0660: */    0,

/* 0x0661: */ 0x03,    0, 0x31, 0x30, 0x30,    0,

/* 0x0667: */    0,    0,    0,    0,    0,    0, 0x02,    0, 0x02, /* "..100..........." */
/* 0x0670: */    0, 0x10,    0,    0, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0, 0x01,    0, 0xae, 0x01, /* ".......\9......." */
/* 0x0680: */ 0x12, 0x02,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x10,    0,    0, 0xff, /* "................" */
/* 0x0690: */ 0xe4, 0x5c, 0x39,    0,    0,    0,    0, 0x02,    0, 0xae, 0x01, 0x44, 0x02,    0,    0,    0, /* "..\9........D..." */
/* 0x06a0: */    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, /* "................" */
/* 0x06b0: */    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, /* "................" */
};


static const struct partDescriptor resistorDescriptor = {
	.type = "res",
	.REF{{{0, 17}, {1, posCENTER}}, {{0, 3}, {1, -5}}, "Part Reference", "R1", 1, 0},
	.VAL{{{0, 17}, {1, posCENTER}}, {{0, 15}, {1, 14}}, "Value", "", 1, 0},
	.pos{9, 5, 45, 65, 10, 20},
	.bb{20, 30},
	.rot = 0,
	.fields{},
	.lib = "",
	.entries{
		{ .t = eCONST, .d = &resistor[0], .s = 1 },
		{ .t = eLIB_PATH, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &resistor[0x3a], .s = 0x243 - 0x3a },
		{ .t = eLIB_PATH, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &resistor[0x27c], .s = 0x2af - 0x27c },
		{ .t = eLIB_PATH, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &resistor[0x2e8], .s = 0x32E - 0x2e8 },
		{ .t = eFIELDS, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &resistor[0x35a], .s = 0x36d - 0x35a },
		{ .t = eLIB_PATH, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &resistor[0x3a6], .s = 0x3c9 - 0x3a6 },
		{ .t = eROTATION, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &resistor[0x3ca], .s = 0x3ce - 0x3ca },
		{ .t = eREF_DESCR, .d = NULL, .s = 0 },
		{ .t = eVAL_DESCR, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &resistor[0x419], .s = 0x41a - 0x419 },
		{ .t = eLIB_PATH, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &resistor[0x453], .s = 0x65C - 0x453 },
		{ .t = eREF_VALUE, .d = NULL, .s = 0 },
		{ .t = eVAL_VALUE, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &resistor[0x667], .s = 0x6c0 - 0x667 },
		{ .t = eNONE, .d = NULL, .s = 0 },
	},
	.graphics{
		{gLINE, {10, 0, 13, 3, 0, 0}},
		{gLINE, {13, 3, 7, 9, 0, 0}},
		{gLINE, {7, 9, 13, 15, 0, 0}},
		{gLINE, {13, 15, 7, 21, 0, 0}},
		{gLINE, {7, 21, 13, 27, 0, 0}},
		{gLINE, {13, 27, 10, 30, 0, 0}},
		{gPIN, {10, 0, 10, -10}},
		{gPIN, {10, 30, 10, 40}},
	},
	.vfields{}
};

static const unsigned char capacitor_np[] = {
/* 0x0000: */ 0x0d, /* LIB */0x36,    0, 0x43, 0x3a, 0x5c, 0x43, 0x41, 0x44, 0x45, 0x4e, 0x43, 0x45, 0x5c, 0x53, 0x50, /* ".6.C:\CADENCE\SP" */
/* 0x0010: */ 0x42, 0x5f, 0x31, 0x36, 0x2e, 0x35, 0x5c, 0x54, 0x4f, 0x4f, 0x4c, 0x53, 0x5c, 0x43, 0x41, 0x50, /* "B_16.5\TOOLS\CAP" */
/* 0x0020: */ 0x54, 0x55, 0x52, 0x45, 0x5c, 0x4c, 0x49, 0x42, 0x52, 0x41, 0x52, 0x59, 0x5c, 0x44, 0x49, 0x53, /* "TURE\LIBRARY\DIS" */
/* 0x0030: */ 0x43, 0x52, 0x45, 0x54, 0x45, 0x2e, 0x4f, 0x4c, 0x42,    0/* end LIB */, 0x18,    0,    0, 0xff, 0xe4, 0x5c, /* "CRETE.OLB......\" */
/* 0x0040: */ 0x39,    0,    0,    0,    0, 0x0d,    0, 0x43, 0x41, 0x50, 0x20, 0x4e, 0x50, 0x2e, 0x4e, 0x6f, /* "9......CAP.NP.No" */
/* 0x0050: */ 0x72, 0x6d, 0x61, 0x6c,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x30,    0,    0,    0, 0x04, /* "rmal.......0...." */
/* 0x0060: */    0, 0x29, 0x0a,    0,    0,    0,    0,    0,    0,    0, 0x0a,    0,    0,    0, 0x02,    0, /* ".).............." */
/* 0x0070: */    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x29, 0x01,    0,    0,    0, 0x07, /* "..........)....." */
/* 0x0080: */    0,    0,    0, 0x13,    0,    0,    0, 0x07,    0,    0,    0,    0,    0,    0,    0,    0, /* "................" */
/* 0x0090: */    0,    0,    0, 0x29, 0x01,    0,    0,    0, 0x02,    0,    0,    0, 0x13,    0,    0,    0, /* "...)............" */
/* 0x00a0: */ 0x02,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x29, 0x0a,    0,    0, /* "............)..." */
/* 0x00b0: */    0, 0x0a,    0,    0,    0, 0x0a,    0,    0,    0, 0x07,    0,    0,    0,    0,    0,    0, /* "................" */
/* 0x00c0: */    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x14,    0, 0x0a,    0, 0xff, 0x7f, 0x03, /* "................" */
/* 0x00d0: */    0, 0x02,    0, 0x02,    0, 0x1a,    0,    0, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0, /* "..........\9...." */
/* 0x00e0: */ 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x01,    0, 0x31,    0, 0x0a,    0,    0,    0,    0,    0, /* "........1......." */
/* 0x00f0: */    0,    0, 0x0a,    0,    0,    0, 0xf6, 0xff, 0xff, 0xff, 0x20,    0, 0x69, 0x02, 0x04,    0, /* "............i..." */
/* 0x0100: */    0,    0, 0x1a, 0x81, 0x69, 0x02,    0,    0, 0x1a,    0,    0, 0xff, 0xe4, 0x5c, 0x39,    0, /* "....i........\9." */
/* 0x0110: */    0,    0,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x01,    0, 0x32,    0, 0x0a,    0,    0, /* "...........2...." */
/* 0x0120: */    0, 0x0a,    0,    0,    0, 0x0a,    0,    0,    0, 0x14,    0,    0,    0, 0x20,    0,    0, /* "................" */
/* 0x0130: */    0, 0x04,    0,    0,    0, 0x1a,    0,    0,    0,    0,    0, 0x02,    0, 0x27, 0xff, 0xe4, /* ".............'.." */
/* 0x0140: */ 0x5c, 0x39,    0,    0,    0,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x0e,    0, 0x50, 0x61, /* "\9............Pa" */
/* 0x0150: */ 0x72, 0x74, 0x20, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65,    0, 0x1d,    0,    0, /* "rt.Reference...." */
/* 0x0160: */    0,    0,    0, 0x30, 0xe9, 0x01,    0, 0x27, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0, /* "...0...'..\9...." */
/* 0x0170: */ 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x05,    0, 0x56, 0x61, 0x6c, 0x75, 0x65,    0, 0x1d,    0, /* "........Value..." */
/* 0x0180: */ 0x09,    0,    0,    0, 0x30,    0, 0x01,    0,    0,    0,    0,    0,    0,    0, 0x01,    0, /* "....0..........." */
/* 0x0190: */ 0x43,    0,    0,    0,    0, 0x06,    0, /* LIB */0x36,    0, 0x43, 0x3a, 0x5c, 0x43, 0x41, 0x44, 0x45, /* "C......6.C:\CADE" */
/* 0x01a0: */ 0x4e, 0x43, 0x45, 0x5c, 0x53, 0x50, 0x42, 0x5f, 0x31, 0x36, 0x2e, 0x35, 0x5c, 0x54, 0x4f, 0x4f, /* "NCE\SPB_16.5\TOO" */
/* 0x01b0: */ 0x4c, 0x53, 0x5c, 0x43, 0x41, 0x50, 0x54, 0x55, 0x52, 0x45, 0x5c, 0x4c, 0x49, 0x42, 0x52, 0x41, /* "LS\CAPTURE\LIBRA" */
/* 0x01c0: */ 0x52, 0x59, 0x5c, 0x44, 0x49, 0x53, 0x43, 0x52, 0x45, 0x54, 0x45, 0x2e, 0x4f, 0x4c, 0x42,    0/* end LIB */, /* "RY\DISCRETE.OLB." */
/* 0x01d0: */ 0x06, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0, 0x06,    0, 0x43, 0x41, 0x50, 0x20, 0x4e, /* "...\9......CAP.N" */
/* 0x01e0: */ 0x50,    0, 0x06,    0, 0x43, 0x41, 0x50, 0x20, 0x4e, 0x50,    0, 0x01,    0, 0x0d,    0, 0x43, /* "P...CAP.NP.....C" */
/* 0x01f0: */ 0x41, 0x50, 0x20, 0x4e, 0x50, 0x2e, 0x4e, 0x6f, 0x72, 0x6d, 0x61, 0x6c,    0, /* LIB */0x36,    0, 0x43, /* "AP.NP.Normal.6.C" */
/* 0x0200: */ 0x3a, 0x5c, 0x43, 0x41, 0x44, 0x45, 0x4e, 0x43, 0x45, 0x5c, 0x53, 0x50, 0x42, 0x5f, 0x31, 0x36, /* ":\CADENCE\SPB_16" */
/* 0x0210: */ 0x2e, 0x35, 0x5c, 0x54, 0x4f, 0x4f, 0x4c, 0x53, 0x5c, 0x43, 0x41, 0x50, 0x54, 0x55, 0x52, 0x45, /* ".5\TOOLS\CAPTURE" */
/* 0x0220: */ 0x5c, 0x4c, 0x49, 0x42, 0x52, 0x41, 0x52, 0x59, 0x5c, 0x44, 0x49, 0x53, 0x43, 0x52, 0x45, 0x54, /* "\LIBRARY\DISCRET" */
/* 0x0230: */ 0x45, 0x2e, 0x4f, 0x4c, 0x42,    0/* end LIB */, 0x1f,    0,    0, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0, /* "E.OLB......\9..." */
/* 0x0240: */    0, 0x06,    0, 0x43, 0x41, 0x50, 0x20, 0x4e, 0x50,    0, 0x01,    0, 0x43,    0,    0,    0, /* "...CAP.NP...C..." */
/* 0x0250: */    0,    0,    0,    0, 0x01,    0, 0x20, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0,    0, /* ".........\9....." */
/* 0x0260: */    0,    0, 0x06,    0, 0x43, 0x41, 0x50, 0x20, 0x4e, 0x50,    0, 0x02,    0, 0x01,    0, 0x31, /* "....CAP.NP.....1" */
/* 0x0270: */    0, 0x7f, 0x01,    0, 0x32,    0, 0x7f, 0x0d, 0x02,    0, 0x03,    0, 0x50, 0x2f, 0x4e,    0, /* "....2.......P/N." */
/* 0x0280: */ /* PN */0x0a,    0, 0x31, 0x36, 0x31, 0x43, 0x36, 0x34, 0x39, 0x31, 0x31, 0x53,    0/* end PN */, 0x0d,    0, 0x50, /* "..161C64911S...P" */
/* 0x0290: */ 0x43, 0x42, 0x20, 0x46, 0x6f, 0x6f, 0x74, 0x70, 0x72, 0x69, 0x6e, 0x74,    0, /* FP */0x04,    0, 0x30, /* "CB.Footprint...0" */
/* 0x02a0: */ 0x34, 0x30, 0x32,    0/* end FP */, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0, 0x08,    0, 0x49, 0x4e, /* "402...\9......IN" */
/* 0x02b0: */ 0x53, 0x35, 0x31, 0x33, 0x35, 0x39,    0, /* LIB */0x36,    0, 0x43, 0x3a, 0x5c, 0x43, 0x41, 0x44, 0x45, /* "S51359.6.C:\CADE" */
/* 0x02c0: */ 0x4e, 0x43, 0x45, 0x5c, 0x53, 0x50, 0x42, 0x5f, 0x31, 0x36, 0x2e, 0x35, 0x5c, 0x54, 0x4f, 0x4f, /* "NCE\SPB_16.5\TOO" */
/* 0x02d0: */ 0x4c, 0x53, 0x5c, 0x43, 0x41, 0x50, 0x54, 0x55, 0x52, 0x45, 0x5c, 0x4c, 0x49, 0x42, 0x52, 0x41, /* "LS\CAPTURE\LIBRA" */
/* 0x02e0: */ 0x52, 0x59, 0x5c, 0x44, 0x49, 0x53, 0x43, 0x52, 0x45, 0x54, 0x45, 0x2e, 0x4f, 0x4c, 0x42,    0/* end LIB */, /* "RY\DISCRETE.OLB." */
/* 0x02f0: */ 0x0d,    0, 0x43, 0x41, 0x50, 0x20, 0x4e, 0x50, 0x2e, 0x4e, 0x6f, 0x72, 0x6d, 0x61, 0x6c,    0, /* "..CAP.NP.Normal." */
/* 0x0300: */ 0xa9, 0xc8,    0,    0, 0xeb,    0, 0xdb,    0, 0x13, 0x01, 0xfe,    0, 0xdc,    0, 0xfa,    0, /* "................" */
/* 0x0310: */ 0x30,    /* ROT */0, 0x0d,    0, 0x02,    0, 0x27, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0, 0xff, /* "0.....'..\9....." */
/* 0x0320: */ 0x7f, 0x03,    0, 0x02,    0, 0x0e,    0, 0x50, 0x61, 0x72, 0x74, 0x20, 0x52, 0x65, 0x66, 0x65, /* ".......Part.Refe" */
/* 0x0330: */ 0x72, 0x65, 0x6e, 0x63, 0x65,    0, 0x0e,    0, 0xf7, 0xff,    0,    0, 0x30, 0x2d, 0x01,    0, /* "rence.......0-.." */
/* 0x0340: */ 0x27, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x05, /* "'..\9..........." */
/* 0x0350: */    0, 0x56, 0x61, 0x6c, 0x75, 0x65,    0, 0x0e,    0, 0x08,    0,    0,    0, 0x30,    0, 0x01, /* ".Value.......0.." */
/* 0x0360: */    0, 0x18, /* LIB */0x36,    0, 0x43, 0x3a, 0x5c, 0x43, 0x41, 0x44, 0x45, 0x4e, 0x43, 0x45, 0x5c, 0x53, /* "..6.C:\CADENCE\S" */
/* 0x0370: */ 0x50, 0x42, 0x5f, 0x31, 0x36, 0x2e, 0x35, 0x5c, 0x54, 0x4f, 0x4f, 0x4c, 0x53, 0x5c, 0x43, 0x41, /* "PB_16.5\TOOLS\CA" */
/* 0x0380: */ 0x50, 0x54, 0x55, 0x52, 0x45, 0x5c, 0x4c, 0x49, 0x42, 0x52, 0x41, 0x52, 0x59, 0x5c, 0x44, 0x49, /* "PTURE\LIBRARY\DI" */
/* 0x0390: */ 0x53, 0x43, 0x52, 0x45, 0x54, 0x45, 0x2e, 0x4f, 0x4c, 0x42,    0/* end LIB */, 0x18,    0,    0, 0xff, 0xe4, /* "SCRETE.OLB......" */
/* 0x03a0: */ 0x5c, 0x39,    0,    0,    0,    0, 0x0d,    0, 0x43, 0x41, 0x50, 0x20, 0x4e, 0x50, 0x2e, 0x4e, /* "\9......CAP.NP.N" */
/* 0x03b0: */ 0x6f, 0x72, 0x6d, 0x61, 0x6c,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x30,    0,    0,    0, /* "ormal.......0..." */
/* 0x03c0: */ 0x04,    0, 0x29, 0x0a,    0,    0,    0,    0,    0,    0,    0, 0x0a,    0,    0,    0, 0x02, /* "..)............." */
/* 0x03d0: */    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x29, 0x01,    0,    0,    0, /* "...........)...." */
/* 0x03e0: */ 0x07,    0,    0,    0, 0x13,    0,    0,    0, 0x07,    0,    0,    0,    0,    0,    0,    0, /* "................" */
/* 0x03f0: */    0,    0,    0,    0, 0x29, 0x01,    0,    0,    0, 0x02,    0,    0,    0, 0x13,    0,    0, /* "....)..........." */
/* 0x0400: */    0, 0x02,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x29, 0x0a,    0, /* ".............).." */
/* 0x0410: */    0,    0, 0x0a,    0,    0,    0, 0x0a,    0,    0,    0, 0x07,    0,    0,    0,    0,    0, /* "................" */
/* 0x0420: */    0,    0,    0,    0,    0,    0,    0,    0,    0,    0, 0x14,    0, 0x0a,    0, 0xff, 0x7f, /* "................" */
/* 0x0430: */ 0x03,    0, 0x02,    0, 0x02,    0, 0x1a,    0,    0, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0, /* "...........\9..." */
/* 0x0440: */    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x01,    0, 0x31,    0, 0x0a,    0,    0,    0,    0, /* ".........1......" */
/* 0x0450: */    0,    0,    0, 0x0a,    0,    0,    0, 0xf6, 0xff, 0xff, 0xff, 0x20,    0, 0x69, 0x02, 0x04, /* ".............i.." */
/* 0x0460: */    0,    0,    0, 0x1a, 0x81, 0x69, 0x02,    0,    0, 0x1a,    0,    0, 0xff, 0xe4, 0x5c, 0x39, /* ".....i........\9" */
/* 0x0470: */    0,    0,    0,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x01,    0, 0x32,    0, 0x0a,    0, /* "............2..." */
/* 0x0480: */    0,    0, 0x0a,    0,    0,    0, 0x0a,    0,    0,    0, 0x14,    0,    0,    0, 0x20,    0, /* "................" */
/* 0x0490: */    0,    0, 0x04,    0,    0,    0, 0x1a,    0,    0,    0,    0,    0, 0x02,    0, 0x27, 0xff, /* "..............'." */
/* 0x04a0: */ 0xe4, 0x5c, 0x39,    0,    0,    0,    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x0e,    0, 0x50, /* ".\9............P" */
/* 0x04b0: */ 0x61, 0x72, 0x74, 0x20, 0x52, 0x65, 0x66, 0x65, 0x72, 0x65, 0x6e, 0x63, 0x65,    0, 0x1d,    0, /* "art.Reference..." */
/* 0x04c0: */    0,    0,    0,    0, 0x30, 0xe9, 0x01,    0, 0x27, 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0, /* "....0...'..\9..." */
/* 0x04d0: */    0, 0xff, 0x7f, 0x03,    0, 0x02,    0, 0x05,    0, 0x56, 0x61, 0x6c, 0x75, 0x65,    0, 0x1d, /* ".........Value.." */
/* 0x04e0: */    0, 0x09,    0,    0,    0, 0x30,    0, 0x01,    0,    0,    0,    0,    0,    0,    0, 0x01, /* ".....0.........." */
/* 0x04f0: */    0, 0x43,    0,    0,    0,    0, 0x06,    0, 0x02,    0, 0x43, 0x31,    0, /* VAL */0x03,    0, 0x43, /* ".C........C1...C" */
/* 0x0500: */ 0x41, 0x50,    0/* end VAL */,    0,    0,    0,    0,    0,    0, 0x02,    0, 0x02,    0, 0x10,    0,    0, /* "AP.............." */
/* 0x0510: */ 0xff, 0xe4, 0x5c, 0x39,    0,    0,    0,    0, 0x01,    0, 0xe6,    0, 0xf0,    0,    0,    0, /* "..\9............" */
/* 0x0520: */    0,    0,    0,    0,    0,    0,    0,    0, 0x10,    0,    0, 0xff, 0xe4, 0x5c, 0x39,    0, /* ".............\9." */
/* 0x0530: */    0,    0,    0, 0x02,    0, 0xe6,    0, 0x0e, 0x01,    0,    0,    0,    0,    0,    0,    0, /* "................" */
/* 0x0540: */    0,    0,    0,    0,    0,    0,    0, 0x08,    0,    0, 0xdb, 0xcc,    0, 0x01,    0, 0x08, /* "................" */
};

static const struct partDescriptor cap_npDescriptor = {
	.type = "cnp",
	.REF{{{0, 14}, {1, posCENTER}}, {{0, -9}, {1, -11}}, "Part Reference", "C1", 1, 0},
	.VAL{{{0, 14}, {1, posCENTER}}, {{0, 8}, {1, 20}}, "Value", "", 1, 0},
	.pos{9, 5, 119, 45, 10, 20},
	.bb{20, 10},
	.rot = 0,
	.fields{},
	.lib = "",
	.entries{
		{ .t = eCONST, .d = &capacitor_np[0], .s = 1 },
		{ .t = eLIB_PATH, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &capacitor_np[0x3a], .s = 0x197 - 0x3a },
		{ .t = eLIB_PATH, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &capacitor_np[0x1d0], .s = 0x1fd - 0x1d0 },
		{ .t = eLIB_PATH, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &capacitor_np[0x236], .s = 0x278 - 0x236 },
		{ .t = eFIELDS, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &capacitor_np[0x2a4], .s = 0x2b7 - 0x2a4 },
		{ .t = eLIB_PATH, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &capacitor_np[0x2f0], .s = 0x311 - 0x2f0 },
		{ .t = eROTATION, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &capacitor_np[0x312], .s = 0x316 - 0x312 },
		{ .t = eREF_DESCR, .d = NULL, .s = 0 },
		{ .t = eVAL_DESCR, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &capacitor_np[0x361], .s = 0x362 - 0x361 },
		{ .t = eLIB_PATH, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &capacitor_np[0x39b], .s = 0x4f8 - 0x39b },
		{ .t = eREF_VALUE, .d = NULL, .s = 0 },
		{ .t = eVAL_VALUE, .d = NULL, .s = 0 },
		{ .t = eCONST, .d = &capacitor_np[0x503], .s = 0x550 - 0x503 },
		{ .t = eNONE, .d = NULL, .s = 0 },
	},
	.graphics{
		{gLINE, {10, 0, 10, 2, 0, 0}},
		{gLINE, {1, 7, 19, 7, 0, 0}},
		{gLINE, {1, 2, 19, 2, 0, 0}},
		{gLINE, {10, 10, 10, 7, 0, 0}},
		{gPIN, {10, 0, 10, -10}},
		{gPIN, {10, 10, 10, 20}},
	},
	.vfields{}
};

class DrawPanel : public wxPanel
{
	public:
		DrawPanel(wxWindow * parent, int id, const wxPoint &pos, const wxSize &size)
			: wxPanel(parent, id, pos, size)
			  , m_w(0)
			  , m_h(0)
		{
			this->SetBackgroundStyle(wxBG_STYLE_PAINT);
		};
		~DrawPanel() {};
		void drawPart(struct partDescriptor &p)
		{
			this->setPart(p);
			this->redraw();
			p.REF.cx = this->m_part.REF.cx;
			p.REF.cy = this->m_part.REF.cy;
			p.VAL.cx = this->m_part.VAL.cx;
			p.VAL.cy = this->m_part.VAL.cy;
		}
		void setPart(const struct partDescriptor &p)
		{
			this->m_part = p;
			this->m_part.pos.center(this->m_w / 2, this->m_h / 2);
		}
		void redraw()
		{
			this->draw();
			this->Refresh();
		};
	protected:
		void OnSize(wxSizeEvent &event)
		{
			wxSize size = event.GetSize();
			if (this->m_w != size.GetWidth() || this->m_h != size.GetHeight()) {
				this->m_w = size.GetWidth();
				this->m_h = size.GetHeight();
				this->mbmp_draw.Create(this->m_w, this->m_h);
				this->draw();
				this->Refresh();
			}
		};
		void OnPaint(wxPaintEvent &event)
		{
			wxDC * dc = new wxBufferedPaintDC(this);
			dc->DrawBitmap(this->mbmp_draw, 0, 0, false);
			delete dc;
		};
	private:
		void draw()
		{
			int w = this->m_w;
			int h = this->m_h;
			if (!this->mbmp_draw.IsOk()) return;
			wxMemoryDC dc(this->mbmp_draw);
			dc.SetBackground(*wxTheBrushList->FindOrCreateBrush(*wxWHITE, wxBRUSHSTYLE_SOLID));
			dc.Clear();
			dc.SetPen(wxPen(*wxBLACK, 1, wxPENSTYLE_SOLID));
			dc.DrawLine(0, 0, 0, h - 1);
			dc.DrawLine(w - 1, 0, w - 1, h - 1);
			dc.DrawLine(0, 0, w - 1, 0);
			dc.DrawLine(0, h - 1, w - 1, h - 1);
			if (this->m_part.pos.is_valid()) {
				wxColour pp_colour(0xB0, 0xB0, 0xB0, wxALPHA_OPAQUE);
				dc.SetPen(wxPen(pp_colour, 1, wxPENSTYLE_SHORT_DASH));
				this->m_part.pos.draw(&dc);
				dc.SetPen(wxPen(pp_colour, 1, wxPENSTYLE_SOLID));
				dc.DrawLine(this->m_part.pos.x3 - 2, this->m_part.pos.y3 - 2, this->m_part.pos.x3 + 3, this->m_part.pos.y3 + 3);
				dc.DrawLine(this->m_part.pos.x3 - 2, this->m_part.pos.y3 + 2, this->m_part.pos.x3 + 3, this->m_part.pos.y3 - 3);
				dc.SetPen(wxPen(pp_colour, 1, wxPENSTYLE_DOT));
				this->m_part.bb.draw(&dc, this->m_part.pos, this->m_part.rot);
				this->m_part.REF.draw(&dc, this->m_part.pos, this->m_part.bb, this->m_part.rot);
				this->m_part.VAL.draw(&dc, this->m_part.pos, this->m_part.bb, this->m_part.rot);
				for (const auto &g : this->m_part.graphics) {
					g.draw(&dc, this->m_part.pos, this->m_part.bb, this->m_part.rot);
				}
			} else {
				dc.DrawText("Part not valid", 0, 0);
			}
		};

		int m_w, m_h;
		wxBitmap mbmp_draw;
		struct partDescriptor m_part;

		wxDECLARE_EVENT_TABLE();
};

wxBEGIN_EVENT_TABLE(DrawPanel, wxPanel)
	EVT_SIZE(DrawPanel::OnSize)
	EVT_PAINT(DrawPanel::OnPaint)
wxEND_EVENT_TABLE()

class MyApp : public wxApp
{
public:
    virtual bool OnInit();
};

#if USE_ASYNCHRONOUS_CLIPBOARD_REQUEST
enum AsyncRequestState
{
   Idle,
   Waiting,
   Finished
};
#endif

class MyFrame : public ClipboardWatcher<wxFrame>
{
public:
    MyFrame(const wxString& title);
	~MyFrame()
	{
		if (this->m_md_res) pcre2_match_data_free(this->m_md_res);
		if (this->m_re_res) pcre2_code_free(this->m_re_res);
		if (this->m_md_cap) pcre2_match_data_free(this->m_md_cap);
		if (this->m_re_cap) pcre2_code_free(this->m_re_cap);
		if (this->m_md_cap_volt) pcre2_match_data_free(this->m_md_cap_volt);
		if (this->m_re_cap_volt) pcre2_code_free(this->m_re_cap_volt);
		if (this->m_md_cap_tol) pcre2_match_data_free(this->m_md_cap_tol);
		if (this->m_re_cap_tol) pcre2_code_free(this->m_re_cap_tol);
		if (this->m_md_cap_fp) pcre2_match_data_free(this->m_md_cap_fp);
		if (this->m_re_cap_fp) pcre2_code_free(this->m_re_cap_fp);
	}

    void OnQuit(wxCommandEvent&event);
    void OnAbout(wxCommandEvent&event);
    void OnAnalyzeText(wxCommandEvent&event);
    void OnCompare(wxCommandEvent&event);
    void OnAnalyzeData(wxCommandEvent&event);
    void OnReplace(wxCommandEvent&event);
    void OnUpdateUIWrite(wxUpdateUIEvent&event);
    void OnUpdateUICmp(wxUpdateUIEvent&event);
    void OnUpdateUIAnalyzeData(wxUpdateUIEvent&event);
    void OnUpdateUIReplace(wxUpdateUIEvent&event);
#if USE_ASYNCHRONOUS_CLIPBOARD_REQUEST
    void OnClipboardChange(wxClipboardEvent&event);
#endif

    virtual void OnClipboardChange();

private:
    void replace();
	void replaceClipboardPart(struct partDescriptor &part);
    void construct_part(void *buf, const struct partDescriptor &part);
    bool isCLERPheader(const wxString &s, size_t * positions = NULL);
    bool parse_const(const void * buf, size_t *idx, const unsigned char * bytes, size_t size);
    bool parse_string(const void * buf, size_t *idx, std::string *out = NULL);
    bool parse_graphic(const void * buf, size_t *idx, struct partDescriptor *part = NULL);
    bool parse_part(const void * buf, size_t *idx);
    bool parse_wire(const void * buf, size_t *idx);
    bool parse_power(const void * buf, size_t *idx);
    bool parse_offpage(const void * buf, size_t *idx);
    bool parse_hier(const void * buf, size_t *idx);
    bool parse_text(const void * buf, size_t *idx);
    bool parse_line(const void * buf, size_t *idx);
	void parse_bytes(const void * buf, size_t *idx, size_t size, int line, const unsigned char * exp = NULL);
	bool parse_rec27(const void * buf, size_t *idx, const char * prefix = NULL, std::list<struct partText *> * texts = NULL);
	bool parse_rec20(const void * buf, size_t *idx, const char * prefix = NULL);
	bool parse_rec18(const void * buf, size_t *idx, const char * prefix = NULL, struct partDescriptor *part = NULL);
	bool parse_name_value_pairs(const void * buf, size_t *idx, const char * prefix = NULL);
	std::map<std::string, std::string> getRes(const std::vector<std::string> & fvec, size_t * positions);
	std::map<std::string, std::string> getCnp(const std::vector<std::string> & fvec, size_t * positions);
	void makeValue();
    wxString getDISCRETE_OLB();
    wxTextCtrl        *m_textctrl;
#if USE_ASYNCHRONOUS_CLIPBOARD_REQUEST
    AsyncRequestState  m_request;
    bool               m_clipboardSupportsText;
#endif
    wxDataFormat m_orcadFormat;
    unsigned char * m_prevData;
    bool m_autoReplace;
    ConfigFrame * m_cfgFrame;
	DrawPanel * m_drawPanel;
	wxToggleButton* mtgl_Rotate;

	std::list<std::tuple<wxToggleButton*, wxTextCtrl*, wxTextCtrl*> > m_userProperties;
	struct partDescriptor m_curPart;
	std::vector<std::string> m_resVFnames, m_cnpVFnames;
	std::map<std::string, wxToggleButton*> m_resVFtgl, m_cnpVFtgl;
	std::map<std::string, wxTextCtrl*> m_resVFtxt, m_cnpVFtxt;

	pcre2_code * m_re_res;
	pcre2_match_data * m_md_res;
	pcre2_code * m_re_cap;
	pcre2_match_data * m_md_cap;
	pcre2_code * m_re_cap_volt;
	pcre2_match_data * m_md_cap_volt;
	pcre2_code * m_re_cap_tol;
	pcre2_match_data * m_md_cap_tol;
	pcre2_code * m_re_cap_fp;
	pcre2_match_data * m_md_cap_fp;

    wxDECLARE_EVENT_TABLE();
};

enum
{
    ID_Quit   = wxID_EXIT,
    ID_About  = wxID_ABOUT,
    ID_Write  = 100,
    ID_Text   = 101,
    ID_Compare,
    ID_AnalyzeData,
    ID_Replace,
	ID_Rotate,
	ID_OnTop,
    ID_AutoReplace,
	ID_resVF = 500,
	ID_cnpVF = 510,
	ID_celVF = 520,
	ID_userPropTgl = 570,
    ID_MenuConfig = 600,
    ID_MenuMoreControls,
};

wxBEGIN_EVENT_TABLE(MyFrame, wxFrame)
    EVT_MENU(ID_Quit,  MyFrame::OnQuit)
    EVT_MENU(ID_About, MyFrame::OnAbout)
    EVT_BUTTON(ID_Write, MyFrame::OnAnalyzeText)
    EVT_BUTTON(ID_Compare, MyFrame::OnCompare)
    EVT_BUTTON(ID_AnalyzeData, MyFrame::OnAnalyzeData)
    EVT_BUTTON(ID_Replace, MyFrame::OnReplace)
    EVT_UPDATE_UI(ID_Write, MyFrame::OnUpdateUIWrite)
    EVT_UPDATE_UI(ID_Compare, MyFrame::OnUpdateUICmp)
    EVT_UPDATE_UI(ID_AnalyzeData, MyFrame::OnUpdateUIAnalyzeData)
    EVT_UPDATE_UI(ID_Replace, MyFrame::OnUpdateUIReplace)
#if USE_ASYNCHRONOUS_CLIPBOARD_REQUEST
    EVT_CLIPBOARD_CHANGED(MyFrame::OnClipboardChange)
#endif
wxEND_EVENT_TABLE()

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
    if ( !wxApp::OnInit() )
        return false;

    wxConfig *config = new wxConfig("CLERP2Schematic", "Compulab");
    wxConfigBase::Set(config);
    MyFrame *frame = new MyFrame("CLERP2Schematic");
    frame->Show(true);

    return true;
}

std::map<std::string, std::string> MyFrame::getRes(const std::vector<std::string> & fvec, size_t * positions)
{
	std::map<std::string, std::string> ret;

	auto fmap = match_re(this->m_re_res, this->m_md_res, fvec[positions[1]]);
	if (fmap.empty()) return ret;

	if (!fmap.at("mul").compare("Ohm")) fmap["mul"] = "R";
	else if (!fmap.at("mul").compare("K")) fmap["mul"] = "k";

	ret["VAL"] = fmap.at("val") + fmap.at("mul");
	ret["%"] = fmap["tol"];
	ret["PW"] = fmap["pow"];
	ret["FP"] = fmap["fp"];
	ret["P/N"] = fvec[positions[0]];
	ret["Part Name"] = fvec[positions[1]];
	ret["PCB Footprint"] = fvec[positions[2]];

	return ret;
}

std::map<std::string, std::string> MyFrame::getCnp(const std::vector<std::string> & fvec, size_t * positions)
{
	std::map<std::string, std::string> ret;

	if (fvec[positions[4]].compare("CAPACITORS - CERAMIC") && !fvec[positions[4]].empty()) return ret;
	auto fmap = match_re(this->m_re_cap, this->m_md_cap, fvec[positions[1]]);
	if (fmap.empty()) return ret;

	auto cvmap = match_re(this->m_re_cap_volt, this->m_md_cap_volt, fvec[positions[1]]);
	if (cvmap.empty()) {
		fmap["volt"] = "";
		fmap["vmul"] = "";
	} else {
		fmap["volt"] = cvmap["volt"];
		fmap["vmul"] = cvmap["vmul"];
	}

	auto cfmap = match_re(this->m_re_cap_fp, this->m_md_cap_fp, fvec[positions[1]]);
	if (cfmap.empty()) fmap["fp"] = "";
	else fmap["fp"] = cfmap["fp"];

	auto cpmap = match_re(this->m_re_cap_tol, this->m_md_cap_tol, fvec[positions[1]]);
	if (cpmap.empty()) fmap["tol"] = "";
	else fmap["tol"] = cpmap["tol"];

	std::string mul = fmap["mul"];
	if (!fmap.at("mul").compare("U")) fmap["mul"] = "u";

	if (!fmap.at("vmul").compare("v")) fmap["vmul"] = "V";
	else if (!fmap.at("vmul").compare("KV")) fmap["vmul"] = "kV";

	ret["VAL"] = fmap.at("val") + fmap.at("mul") + "F";
	ret["%"] = fmap.at("tol");
	ret["VR"] = fmap.at("volt") + fmap.at("vmul");
	ret["FP"] = fmap.at("fp");
	ret["P/N"] = fvec[positions[0]];
	ret["Part Name"] = fvec[positions[1]];
	ret["PCB Footprint"] = fvec[positions[2]];

	return ret;
}

MyFrame::MyFrame(const wxString& title)
	: ClipboardWatcher((wxWindow*)NULL, wxID_ANY, title)
	, m_orcadFormat("OrCad Schematic Page Editor")
	, m_prevData(NULL)
	  , m_autoReplace(false)
	  , m_re_res(NULL)
	  , m_md_res(NULL)
	  , m_re_cap(NULL)
	  , m_md_cap(NULL)
	  , m_re_cap_volt(NULL)
	  , m_md_cap_volt(NULL)
	  , m_re_cap_tol(NULL)
	  , m_md_cap_tol(NULL)
	  , m_re_cap_fp(NULL)
	  , m_md_cap_fp(NULL)
{
	wxConfigBase *cfg = wxConfigBase::Get();

	{
		wxString pat = "^Res\\.?,\\s+(?<full>(?:Current sense, )?(?:Axial, )?(?<val>[0-9.]+)\\s*(?<mul>[kKMR]|Ohm)(?:/Jumper)?(?:,\\s+(?<tol>[0-9.]+%))?(?:,\\s+(?<fp>TH|[0-9]+|[0-9.x]+mm)(?:\\s+\\(DxL\\))?)?(?:,\\s+(?<pow>[0-9./]+W))?.*)";
		PCRE2_SPTR pattern = (PCRE2_SPTR)pat.c_str();
		int errornumber;
		PCRE2_SIZE erroroffset;
		this->m_re_res = pcre2_compile(
			pattern,               /* the pattern */
			PCRE2_ZERO_TERMINATED, /* indicates pattern is zero-terminated */
			0,                     /* default options */
			&errornumber,          /* for error number */
			&erroroffset,          /* for error offset */
			NULL);                 /* use default compile context */
		if (this->m_re_res == NULL) {
			/* Compilation failed */
			PCRE2_UCHAR buffer[256];
			pcre2_get_error_message(errornumber, buffer, sizeof(buffer));
			wxMessageBox(wxString::Format("PCRE2 compilation failed at offset %d: %s", (int)erroroffset, (char *)buffer),
						 "Error", wxICON_ERROR, this);
		} else {
			this->m_md_res = pcre2_match_data_create_from_pattern(this->m_re_res, NULL);
		}
	}

	{
		wxString pat = "^Cap\\.,\\s+(?<rf>RF,\\s+)?(?<full>(?<val>[0-9.]+)(?<mul>[pnuU])?F.*)";
		PCRE2_SPTR pattern = (PCRE2_SPTR)pat.c_str();
		int errornumber;
		PCRE2_SIZE erroroffset;
		this->m_re_cap = pcre2_compile(
			pattern,               /* the pattern */
			PCRE2_ZERO_TERMINATED, /* indicates pattern is zero-terminated */
			0,                     /* default options */
			&errornumber,          /* for error number */
			&erroroffset,          /* for error offset */
			NULL);                 /* use default compile context */
		if (this->m_re_cap == NULL) {
			/* Compilation failed */
			PCRE2_UCHAR buffer[256];
			pcre2_get_error_message(errornumber, buffer, sizeof(buffer));
			wxMessageBox(wxString::Format("PCRE2 cap compilation failed at offset %d: %s", (int)erroroffset, (char *)buffer),
						 "Error", wxICON_ERROR, this);
		} else {
			this->m_md_cap = pcre2_match_data_create_from_pattern(this->m_re_cap, NULL);
		}
	}

	{
		wxString pat = "[, ](?<volt>(?:=>)?[0-9.]+)(?<vmul>v|V|KV|kV)\\b";
		PCRE2_SPTR pattern = (PCRE2_SPTR)pat.c_str();
		int errornumber;
		PCRE2_SIZE erroroffset;
		this->m_re_cap_volt = pcre2_compile(
			pattern,               /* the pattern */
			PCRE2_ZERO_TERMINATED, /* indicates pattern is zero-terminated */
			0,                     /* default options */
			&errornumber,          /* for error number */
			&erroroffset,          /* for error offset */
			NULL);                 /* use default compile context */
		if (this->m_re_cap_volt == NULL) {
			/* Compilation failed */
			PCRE2_UCHAR buffer[256];
			pcre2_get_error_message(errornumber, buffer, sizeof(buffer));
			wxMessageBox(wxString::Format("PCRE2 capv compilation failed at offset %d: %s", (int)erroroffset, (char *)buffer),
						 "Error", wxICON_ERROR, this);
		} else {
			this->m_md_cap_volt = pcre2_match_data_create_from_pattern(this->m_re_cap_volt, NULL);
		}
	}

	{
		wxString pat = "[ ,](?<tol>\\+?\\d+(?:(?:-|,-)\\d+)?%|\\+-[0-9.]+pF)";
		PCRE2_SPTR pattern = (PCRE2_SPTR)pat.c_str();
		int errornumber;
		PCRE2_SIZE erroroffset;
		this->m_re_cap_tol = pcre2_compile(
			pattern,               /* the pattern */
			PCRE2_ZERO_TERMINATED, /* indicates pattern is zero-terminated */
			0,                     /* default options */
			&errornumber,          /* for error number */
			&erroroffset,          /* for error offset */
			NULL);                 /* use default compile context */
		if (this->m_re_cap_tol == NULL) {
			/* Compilation failed */
			PCRE2_UCHAR buffer[256];
			pcre2_get_error_message(errornumber, buffer, sizeof(buffer));
			wxMessageBox(wxString::Format("PCRE2 capp compilation failed at offset %d: %s", (int)erroroffset, (char *)buffer),
						 "Error", wxICON_ERROR, this);
		} else {
			this->m_md_cap_tol = pcre2_match_data_create_from_pattern(this->m_re_cap_tol, NULL);
		}
	}

	{
		wxString pat = "[ ,](?<fp>\\d{4,6}|TH)(?:,| |$)";
		PCRE2_SPTR pattern = (PCRE2_SPTR)pat.c_str();
		int errornumber;
		PCRE2_SIZE erroroffset;
		this->m_re_cap_fp = pcre2_compile(
			pattern,               /* the pattern */
			PCRE2_ZERO_TERMINATED, /* indicates pattern is zero-terminated */
			0,                     /* default options */
			&errornumber,          /* for error number */
			&erroroffset,          /* for error offset */
			NULL);                 /* use default compile context */
		if (this->m_re_cap_fp == NULL) {
			/* Compilation failed */
			PCRE2_UCHAR buffer[256];
			pcre2_get_error_message(errornumber, buffer, sizeof(buffer));
			wxMessageBox(wxString::Format("PCRE2 capf compilation failed at offset %d: %s", (int)erroroffset, (char *)buffer),
						 "Error", wxICON_ERROR, this);
		} else {
			this->m_md_cap_fp = pcre2_match_data_create_from_pattern(this->m_re_cap_fp, NULL);
		}
	}

    // set the frame icon
    SetIcon(wxICON(sample));

#if USE_ASYNCHRONOUS_CLIPBOARD_REQUEST
    m_request = Idle;
    m_clipboardSupportsText = false;
#endif

#if wxUSE_MENUS
    // create a menu bar
    wxMenu *fileMenu = new wxMenu;

    wxMenu *toolMenu = new wxMenu;
    toolMenu->Append(ID_MenuConfig, "&Config", "Configuration");
    toolMenu->Append(ID_MenuMoreControls, "&Show more controls", "Show additional controls (for debugging)");

    // the "About" item should be in the help menu
    wxMenu *helpMenu = new wxMenu;
    helpMenu->Append(ID_About, "&About\tF1", "Show about dialog");

    fileMenu->Append(ID_Quit, "E&xit\tAlt-X", "Quit this program");

    // now append the freshly created menu to the menu bar...
    wxMenuBar *menuBar = new wxMenuBar();
    menuBar->Append(fileMenu, "&File");
    menuBar->Append(toolMenu, "&Tools");
    menuBar->Append(helpMenu, "&Help");

    // ... and attach this menu bar to the frame
    SetMenuBar(menuBar);
#endif // wxUSE_MENUS

    wxPanel *panel = new wxPanel( this, -1 );

    wxBoxSizer *main_sizer = new wxBoxSizer( wxVERTICAL );
    auto hs = new wxBoxSizer(wxHORIZONTAL);
    main_sizer->Add(hs, 0, wxALL | wxEXPAND, 5);
    hs->Add( new wxButton( panel, ID_Write, "Analyze text" ), 0);
	this->FindWindowById(ID_Write)->Show(false);
    hs->Add( new wxButton( panel, ID_Compare, "Compare" ), 0, wxLEFT, 10 );
	this->FindWindowById(ID_Compare)->Show(false);
    hs->Add( new wxButton( panel, ID_AnalyzeData, "Analyze data" ), 0, wxLEFT, 10 );
	this->FindWindowById(ID_AnalyzeData)->Show(false);
    hs->Add( new wxButton( panel, ID_Replace, "Replace" ), 0, wxLEFT, 10 );
	hs->Add( (this->mtgl_Rotate = new wxToggleButton( panel, ID_Rotate, "Rotate")), 0, wxLEFT, 10);
    hs->Add( new wxToggleButton( panel, ID_OnTop, "On top" ), 0, wxLEFT, 10 );
    hs->Add( new wxCheckBox( panel, ID_AutoReplace, "Auto replace" ), 0, wxLEFT | wxALIGN_CENTER_VERTICAL, 10 );
    m_textctrl = new wxTextCtrl( panel, ID_Text, "", wxDefaultPosition,
      wxDefaultSize, wxTE_MULTILINE | wxTE_RICH | wxTE_RICH2 | wxTE_DONTWRAP );
	this->FindWindowById(ID_Text)->Show(false);
    wxFont font = wxFont(wxSize(10, 10), wxFontFamily::wxFONTFAMILY_TELETYPE, wxFontStyle::wxFONTSTYLE_NORMAL, wxFontWeight::wxFONTWEIGHT_NORMAL);
    m_textctrl->SetFont(font);
    main_sizer->Add( m_textctrl, 1, wxGROW );

	main_sizer->Add((this->m_drawPanel = new DrawPanel(panel, wxID_ANY, wxDefaultPosition, wxSize(160, 120))), 0, wxEXPAND | wxALL, 5);

	/* =================== Value Fields ===================== */
	auto vflds = new wxStaticBoxSizer(wxVERTICAL, panel, "Value fields");
	main_sizer->Add(vflds, 0, wxRIGHT | wxLEFT | wxBOTTOM | wxEXPAND, 5);
	auto vfldsb = vflds->GetStaticBox();
	{
		int id = ID_resVF;
		this->m_resVFnames = std::vector<std::string>{"%", "PW", "FP"};
		auto sz = new wxBoxSizer(wxHORIZONTAL);
		vflds->Add(sz, 0, wxEXPAND | wxRIGHT | wxLEFT | wxBOTTOM | wxTOP, 4);
		sz->Add(new wxStaticText(vfldsb, wxID_ANY, "Res:"), 2, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
		this->m_resVFtxt["VAL"] = new wxTextCtrl(vfldsb, id++, "VAL", wxDefaultPosition, wxSize(30, -1), wxTE_READONLY);
		sz->Add(this->m_resVFtxt.at("VAL"), 3, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
		for (const auto &f: this->m_resVFnames) {
			this->m_resVFtgl[f] = new wxToggleButton(vfldsb, id++, f, wxDefaultPosition, wxSize(30, -1));
			this->m_resVFtxt[f] = new wxTextCtrl(vfldsb, id++, "", wxDefaultPosition, wxSize(30, -1), wxTE_READONLY);
			sz->Add(this->m_resVFtgl.at(f), 2, wxRIGHT, 4);
			sz->Add(this->m_resVFtxt.at(f), 3, wxRIGHT, 4);
			this->m_resVFtgl.at(f)->SetValue(cfg->ReadBool(wxString::Format("/components/res/value/%s", f), false));
		}
	}
	{
		int id = ID_cnpVF;
		this->m_cnpVFnames = std::vector<std::string>{"VR", "%", "FP"};
		auto sz = new wxBoxSizer(wxHORIZONTAL);
		vflds->Add(sz, 0, wxEXPAND | wxRIGHT | wxLEFT | wxBOTTOM | wxTOP, 4);
		sz->Add(new wxStaticText(vfldsb, wxID_ANY, "Cnp:"), 2, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);

		this->m_cnpVFtxt["VAL"] = new wxTextCtrl(vfldsb, id++, "VAL", wxDefaultPosition, wxSize(30, -1), wxTE_READONLY);
		sz->Add(this->m_cnpVFtxt.at("VAL"), 3, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
		for (const auto &f: this->m_cnpVFnames) {
			this->m_cnpVFtgl[f] = new wxToggleButton(vfldsb, id++, f, wxDefaultPosition, wxSize(30, -1));
			this->m_cnpVFtxt[f] = new wxTextCtrl(vfldsb, id++, "", wxDefaultPosition, wxSize(30, -1), wxTE_READONLY);
			sz->Add(this->m_cnpVFtgl.at(f), 2, wxRIGHT, 4);
			sz->Add(this->m_cnpVFtxt.at(f), 3, wxRIGHT, 4);
			this->m_cnpVFtgl.at(f)->SetValue(cfg->ReadBool(wxString::Format("/components/cnp/value/%s", f), f.compare("VR") ? false : true));
		}
	}

	/* =================== User Properties ===================== */
	auto flds = new wxStaticBoxSizer(wxVERTICAL, panel, "User properties");
	main_sizer->Add(flds, 0, wxRIGHT | wxLEFT | wxBOTTOM | wxEXPAND, 5);
	auto fldsb = flds->GetStaticBox();
	for (int i = 0; i < 3; ++i) {
		auto sz = new wxBoxSizer(wxHORIZONTAL);
		flds->Add(sz, 0, wxEXPAND | wxRIGHT | wxLEFT | wxBOTTOM | (i ? 0 : wxTOP), 4);
		wxToggleButton* tgl = new wxToggleButton(fldsb, ID_userPropTgl + i, "EN", wxDefaultPosition, wxSize(30, -1));
		wxTextCtrl* n = new wxTextCtrl(fldsb, wxID_ANY, wxString::Format("CLABCONF%i", i));
		wxTextCtrl* v = new wxTextCtrl(fldsb, wxID_ANY, "");
		sz->Add(tgl, 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
		sz->Add(new wxStaticText(fldsb, wxID_ANY, "Name:"), 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
		sz->Add(n, 1, wxRIGHT, 4);
		sz->Add(new wxStaticText(fldsb, wxID_ANY, "Value:"), 0, wxRIGHT | wxALIGN_CENTER_VERTICAL, 4);
		sz->Add(v, 1, wxRIGHT, 4);
		this->m_userProperties.push_back(std::make_tuple(tgl, n, v));
	}

    panel->SetSizer( main_sizer );

	this->Bind(wxEVT_TOGGLEBUTTON,
			   [this] (wxCommandEvent &e) -> void {
				   if (this->mtgl_Rotate->GetValue() && this->m_curPart.rot == 0) this->m_curPart.rot = 1;
				   else if (!this->mtgl_Rotate->GetValue() && this->m_curPart.rot == 1) this->m_curPart.rot = 0;
				   else return;
				   if (this->m_curPart.entries.size()) {
					   this->replaceClipboardPart(this->m_curPart);
				   }
			   }, ID_Rotate);

	this->Bind(wxEVT_TOGGLEBUTTON,
			   [this] (wxCommandEvent &e) -> void {
				   if (((wxToggleButton*)e.GetEventObject())->GetValue()) {
					   long st = this->GetWindowStyle();
					   st |= wxSTAY_ON_TOP;
					   this->SetWindowStyle(st);
				   } else {
					   long st = this->GetWindowStyle();
					   st &= ~wxSTAY_ON_TOP;
					   this->SetWindowStyle(st);
				   }
			   }, ID_OnTop);

    this->Bind(wxEVT_CHECKBOX,
	    [this] (wxCommandEvent & e) -> void {
		    if (e.IsChecked()) {
		    	this->m_autoReplace = true;
		    } else {
		    	this->m_autoReplace = false;
		    }
	    }, ID_AutoReplace);

	this->Bind(wxEVT_TOGGLEBUTTON,
			   [this] (wxCommandEvent &e) -> void {
				   wxConfigBase *cfg = wxConfigBase::Get();
				   for (const auto &p : this->m_resVFtgl) {
					   if ((void*)p.second == (void*)e.GetEventObject()) {
						   cfg->Write(wxString::Format("/components/res/value/%s", p.first), ((wxToggleButton*)e.GetEventObject())->GetValue());
					   }
				   }
				   if (!this->m_curPart.type.compare("res")) {
					   this->makeValue();
					   if (this->m_curPart.entries.size()) {
						   this->replaceClipboardPart(this->m_curPart);
					   }
				   }
			   }, ID_resVF, ID_cnpVF - 1);

	this->Bind(wxEVT_TOGGLEBUTTON,
			   [this] (wxCommandEvent &e) -> void {
				   wxConfigBase *cfg = wxConfigBase::Get();
				   for (const auto &p : this->m_resVFtgl) {
					   if ((void*)p.second == (void*)e.GetEventObject()) {
						   cfg->Write(wxString::Format("/components/cnp/value/%s", p.first), ((wxToggleButton*)e.GetEventObject())->GetValue());
					   }
				   }
				   if (!this->m_curPart.type.compare("cnp")) {
					   this->makeValue();
					   if (this->m_curPart.entries.size()) {
						   this->replaceClipboardPart(this->m_curPart);
					   }
				   }
			   }, ID_cnpVF, ID_celVF - 1);

    this->m_cfgFrame = new ConfigFrame{this};

    Bind(wxEVT_MENU,
	    [=](wxCommandEvent& e) {
		    this->m_cfgFrame->Show(true);
	    }, ID_MenuConfig);

    Bind(wxEVT_MENU,
	    [=](wxCommandEvent& e) {
			this->FindWindowById(ID_Write)->Show(true);
			this->FindWindowById(ID_Compare)->Show(true);
			this->FindWindowById(ID_AnalyzeData)->Show(true);
			this->FindWindowById(ID_Text)->Show(true);
			this->GetSizer()->Layout();
			this->GetSizer()->Fit(this);
	    }, ID_MenuMoreControls);

    this->BeginWatchingClipboard();

	this->SetSizer(new wxBoxSizer(wxVERTICAL));
	this->GetSizer()->Add(panel, 1, wxEXPAND);
	this->GetSizer()->SetSizeHints(this);
	this->GetSizer()->Layout();
	this->GetSizer()->Fit(this);
}

wxString MyFrame::getDISCRETE_OLB()
{
	wxConfigBase *cfg = wxConfigBase::Get();
	return cfg->Read("/components/lib", "Y:\\CLERP2ORCAD.OLB");
}

void MyFrame::OnAnalyzeText(wxCommandEvent& WXUNUSED(event))
{
   if (wxTheClipboard->Open())
   {
        if (wxTheClipboard->IsSupported( wxDF_UNICODETEXT ))
        {
			wxTextDataObject data;
			wxTheClipboard->GetData( data );
			wxString text = data.GetText();
			m_textctrl->Clear();

			size_t positions[8];

			if (this->isCLERPheader(text.BeforeFirst('\n'), positions)) {
				this->m_textctrl->AppendText("Looks like CLERP part(s)\n");
				text = text.AfterFirst('\n');
			} else {
				this->m_textctrl->AppendText("Not a CLERP part(s)");
				wxTheClipboard->Close();
				return;
			}

			while (!text.empty()) {
				auto fvec = tokenize(text.BeforeFirst('\n').ToStdString(), '\t');
				this->m_textctrl->AppendText(wxString::Format("Part Name: '%s', Family: '%s'", fvec[positions[1]], fvec[positions[4]]));

				std::map<std::string, std::string> fmap;

				if (!(fmap = this->getRes(fvec, positions)).empty()) {
					this->m_textctrl->AppendText("RESISTOR:");
					this->m_textctrl->AppendText(wxString::Format("\tVAL: '%s'", fmap["VAL"]));
					this->m_textctrl->AppendText(wxString::Format("\t%%: '%s'", fmap["%"]));
					this->m_textctrl->AppendText(wxString::Format("\tPW: '%s'", fmap["PW"]));
					this->m_textctrl->AppendText(wxString::Format("\tFP: '%s'", fmap["FP"]));
					this->m_textctrl->AppendText(wxString::Format("\tfull: '%s'\n", fmap["Part Name"]));
					text = text.AfterFirst('\n');
					continue;
				}

				if (!(fmap = this->getCnp(fvec, positions)).empty()) {
					this->m_textctrl->AppendText("CAP NP:");
					this->m_textctrl->AppendText(wxString::Format("\tVAL: '%s'", fmap["VAL"]));
					this->m_textctrl->AppendText(wxString::Format("\tVR: '%s'", fmap["VR"]));
					this->m_textctrl->AppendText(wxString::Format("\t%%: '%s'", fmap["%"]));
					this->m_textctrl->AppendText(wxString::Format("\tFP: '%s'", fmap["FP"]));
					this->m_textctrl->AppendText(wxString::Format("\tfull: '%s'\n", fmap["Part Name"]));
					text = text.AfterFirst('\n');
					continue;
				}

				if (!fvec[positions[4]].compare("CAPACITORS ELECTROLITIC, TANTALUM")) {
					fmap = match_re(this->m_re_cap, this->m_md_cap, fvec[positions[1]]);
					if (!fmap.empty()) {
						auto cvmap = match_re(this->m_re_cap_volt, this->m_md_cap_volt, fvec[positions[1]]);
						if (cvmap.empty()) {
							fmap["volt"] = "";
							fmap["vmul"] = "";
						} else {
							fmap["volt"] = cvmap["volt"];
							fmap["vmul"] = cvmap["vmul"];
						}
						auto cfmap = match_re(this->m_re_cap_fp, this->m_md_cap_fp, fvec[positions[1]]);
						if (cfmap.empty()) fmap["fp"] = "";
						else fmap["fp"] = cfmap["fp"];
						auto cpmap = match_re(this->m_re_cap_tol, this->m_md_cap_tol, fvec[positions[1]]);
						if (cpmap.empty()) fmap["tol"] = "";
						else fmap["tol"] = cpmap["tol"];
						this->m_textctrl->AppendText("P CAPACITOR matched\n");
						this->m_textctrl->AppendText(wxString::Format("\tval: '%s'\n", fmap["val"]));
						this->m_textctrl->AppendText(wxString::Format("\tmul: '%s'\n", fmap["mul"]));
						this->m_textctrl->AppendText(wxString::Format("\tvolt: '%s'\n", fmap["volt"]));
						this->m_textctrl->AppendText(wxString::Format("\ttol: '%s'\n", fmap["tol"]));
						this->m_textctrl->AppendText(wxString::Format("\tfp: '%s'\n", fmap["fp"]));
						this->m_textctrl->AppendText(wxString::Format("\tfull: '%s'\n", fmap["full"]));
						text = text.AfterFirst('\n');
						continue;
					}
				}

				text = text.AfterFirst('\n');
			}

        }
        wxTheClipboard->Close();
   }
}

void MyFrame::OnCompare(wxCommandEvent& WXUNUSED(event))
{
   if (wxTheClipboard->Open())
   {
        if (wxTheClipboard->IsSupported( this->m_orcadFormat ))
        {
		m_textctrl->Clear();
		wxCustomDataObject data{this->m_orcadFormat};
		wxTheClipboard->GetData( data );
		if (data.GetDataSize() != 65535) {
			wxMessageBox(wxString::Format("Data size is not 65535, but %lu", (unsigned long)data.GetDataSize()), "Warning", wxICON_EXCLAMATION);
		}
		unsigned char * d = (unsigned char *)data.GetData();
		size_t dsize = data.GetDataSize();
		if (dsize > 65536) dsize = 65536;
		m_textctrl->Freeze();
		if (!this->m_prevData) {
			this->m_prevData = new unsigned char[65536];
			for (size_t i = 0; i < dsize; i += 16) {
				wxString line = "";
				line.Append(wxString::Format("%04x: ", (int)i));
				for (size_t j = i; j < dsize && j < (i + 16); ++j) {
					line.Append(wxString::Format("%02x ", (int)d[j]));
				}
				for (size_t j = i; j < dsize && j < (i + 16); ++j) {
					line.Append(wxString::Format("%c", (d[j] > 0x20 && d[j] < 0x7F) ? (int)d[j] : '.'));
				}
				line.Append("\n");
				this->m_textctrl->AppendText(line);
			}
		} else {
			for (size_t i = 0; i < dsize; i += 16) {
				size_t l = 16;
				if ((i + l) > dsize) l = dsize - i;
				wxString line = "";
				if (!memcmp(&d[i], &this->m_prevData[i], l)) {
					line.Append(wxString::Format("%04x: ", (int)i));
					for (size_t j = i; j < dsize && j < (i + 16); ++j) {
						line.Append(wxString::Format("%02x ", (int)d[j]));
					}
					for (size_t j = i; j < dsize && j < (i + 16); ++j) {
						line.Append(wxString::Format("%c", (d[j] > 0x20 && d[j] < 0x7F) ? (int)d[j] : '.'));
					}
					line.Append("\n");
					this->m_textctrl->AppendText(line);
				} else {
					size_t last_pos = this->m_textctrl->GetLastPosition();
					auto style = this->m_textctrl->GetDefaultStyle();
					style.SetBackgroundColour(*wxRED);

					line.Append(wxString::Format("%04x: ", (int)i));

					for (size_t j = i; j < dsize && j < (i + 16); ++j) {
						line.Append(wxString::Format("%02x ", (int)this->m_prevData[j]));
					}
					for (size_t j = i; j < dsize && j < (i + 16); ++j) {
						line.Append(wxString::Format("%c", (this->m_prevData[j] > 0x20 && this->m_prevData[j] < 0x7F) ? (int)this->m_prevData[j] : '.'));
					}

					line.Append(" -> ");

					for (size_t j = i; j < dsize && j < (i + 16); ++j) {
						line.Append(wxString::Format("%02x ", (int)d[j]));
					}
					for (size_t j = i; j < dsize && j < (i + 16); ++j) {
						line.Append(wxString::Format("%c", (d[j] > 0x20 && d[j] < 0x7F) ? (int)d[j] : '.'));
					}

					line.Append("\n");

					this->m_textctrl->AppendText(line);

					for (size_t j = i; j < dsize && j < (i + 16); ++j) {
						if (this->m_prevData[j] != d[j]) {
							this->m_textctrl->SetStyle(last_pos + 6 + 3 * (j - i), last_pos + 6 + 3 * (j - i) + 2, style);
							this->m_textctrl->SetStyle(last_pos + 6 + 3 * l + 1 * (j - i), last_pos + 6 + 3 * l + 1 * (j - i) + 1, style);
							this->m_textctrl->SetStyle(last_pos + 6 + 4 * l + 4 + 3 * (j - i), last_pos + 6 + 4 * l + 4 + 3 * (j - i) + 2, style);
							this->m_textctrl->SetStyle(last_pos + 6 + 7 * l + 4 + 1 * (j - i), last_pos + 6 + 7 * l + 4 + 1 * (j - i) + 1, style);
						}
					}
				}
			}
		}
		m_textctrl->ShowPosition(0);
		m_textctrl->Thaw();
		memcpy(this->m_prevData, d, dsize);
        }
        wxTheClipboard->Close();
   }
}

void MyFrame::OnAnalyzeData(wxCommandEvent& WXUNUSED(event))
{
   if (wxTheClipboard->Open())
   {
        if (wxTheClipboard->IsSupported( this->m_orcadFormat ))
        {
		m_textctrl->Clear();
		wxCustomDataObject data{this->m_orcadFormat};
		wxTheClipboard->GetData( data );
		if (data.GetDataSize() != 65535) {
			wxMessageBox(wxString::Format("Data size is not 65535, but %lu", (unsigned long)data.GetDataSize()), "Warning", wxICON_EXCLAMATION);
		}
		unsigned char * d = (unsigned char *)data.GetData();
		size_t dsize = data.GetDataSize();
		if (dsize > 65536) dsize = 65536;
		m_textctrl->Freeze();

		size_t idx = 0;

		try {
			while (d[idx] != 0) {
				int type = d[idx++];
				switch (type) {
					case 0x0d /* PART */:
						if (!this->parse_part(d, &idx)) throw (unsigned long)__LINE__;
						break;
					case 0x14 /* WIRE */:
						if (!this->parse_wire(d, &idx)) throw (unsigned long)__LINE__;
						break;
					case 0x17 /* HIER */:
						if (!this->parse_hier(d, &idx)) throw (unsigned long)__LINE__;
						break;
					case 0x25 /* POWER */:
						if (!this->parse_power(d, &idx)) throw (unsigned long)__LINE__;
						break;
					case 0x26 /* OFFPAGE */:
						if (!this->parse_offpage(d, &idx)) throw (unsigned long)__LINE__;
						break;
					case 0x3d /* TEXT */:
						if (!this->parse_text(d, &idx)) throw (unsigned long)__LINE__;
						break;
					case 0x38 /* LINE */:
						if (!this->parse_line(d, &idx)) throw (unsigned long)__LINE__;
						break;
					case 0x37 /* RECT */:
						this->m_textctrl->AppendText("============ 0x37 RECT =============\n");
						this->parse_bytes(d, &idx, 109, __LINE__);
						break;
					default:
						--idx;
						if (!this->parse_const(d, &idx, (const unsigned char *)("\x00"), 1)) throw (unsigned long)__LINE__;
				}
			}

		} catch (unsigned long l) {
			this->m_textctrl->AppendText(wxString::Format("!!!PARSE ERROR at line %li\n", l));
		}

		size_t end_dump = (idx + 0x1000) < dsize ? idx + 0x1000 : dsize;
		for (; idx < end_dump; idx += 16) {
			wxString line = "";
			line.Append(wxString::Format("%04x: ", (int)idx));
			for (size_t j = idx; j < dsize && j < (idx + 16); ++j) {
				line.Append(wxString::Format("%02x ", (int)d[j]));
			}
			for (size_t j = idx; j < dsize && j < (idx + 16); ++j) {
				line.Append(wxString::Format("%c", (d[j] > 0x20 && d[j] < 0x7F) ? (int)d[j] : '.'));
			}
			line.Append("\n");
			this->m_textctrl->AppendText(line);
		}

		m_textctrl->ShowPosition(0);
		m_textctrl->Thaw();
        }
        wxTheClipboard->Close();
   }
}

bool MyFrame::parse_const(const void * buf, size_t *idx, const unsigned char * bytes, size_t size)
{
	bool wrong = false;
	auto style_wrong = this->m_textctrl->GetDefaultStyle();
	style_wrong.SetBackgroundColour(*wxRED);

	const unsigned char* d = (const unsigned char*) buf;

	wxString t = "CONST:";
	for (size_t i = 0; i < size; ++i) {
		t.Append(wxString::Format(" %.2x", (int)d[*idx + i]));
	}
	this->m_textctrl->AppendText(t);

	size_t last_pos = this->m_textctrl->GetLastPosition();

	for (size_t i = 0; i < size; ++i) {
		if (d[*idx + i] != bytes[i]) {
			wrong = true;
			this->m_textctrl->SetStyle(last_pos - (size - i) * 3 + 1, last_pos - (size - i) * 3 + 3, style_wrong);
		}
	}

	this->m_textctrl->AppendText("\n");


	if (!wrong) *idx += size;
	return !wrong;
}

void MyFrame::parse_bytes(const void * buf, size_t *idx, size_t size, int line, const unsigned char * exp)
{
	auto style_chg = this->m_textctrl->GetDefaultStyle();
	style_chg.SetBackgroundColour(*wxYELLOW);

	const unsigned char* d = (const unsigned char*) buf;
	size_t p = *idx;

	size_t last_pos = this->m_textctrl->GetLastPosition();

	wxString t = wxString::Format("BYTES (%5i):", line);
	last_pos += 15;

	for (size_t i = 0; i < size; ++i) {
		t.Append(wxString::Format(" %.2x", (int)d[p + i]));
	}
	this->m_textctrl->AppendText(t);

	if (exp) {
		bool changes = false;
		for (size_t i = 0; i < size; ++i) {
			if (d[p + i] != exp[i]) {
				changes = true;
				this->m_textctrl->SetStyle(last_pos + 3 * i, last_pos + 2 + 3 * i, style_chg);
			}
		}
		if (changes) {
			t = "\n              ";
			for (size_t i = 0; i < size; ++i) {
				t.Append(wxString::Format(" %.2x", (int)exp[i]));
			}
			this->m_textctrl->AppendText(t);
		}
	}

	this->m_textctrl->AppendText("\n");

	*idx += size;
}

bool MyFrame::parse_string(const void * buf, size_t *idx, std::string *out)
{
	bool wrong = false;
	auto style_wrong = this->m_textctrl->GetDefaultStyle();
	style_wrong.SetBackgroundColour(*wxRED);

	const unsigned char* d = (const unsigned char*) buf;
	size_t p = *idx;

	int str_len = pick_short(d, &p);

	wxString t = wxString::Format("STR (LEN = %04i): '", (int)str_len);

	if (out) out->clear();
	for (int i = 0; i < str_len; ++i) {
		t.Append((char)d[p], 1);
		if (out) out->append(1, (char)d[p]);
		if (d[p] < 10) wrong = true;
		++p;
	}
	t.Append(wxString::Format("' 0x%.2x", (int)d[p]));

	this->m_textctrl->AppendText(t);

	size_t last_pos = this->m_textctrl->GetLastPosition();

	if (d[p] != 0) {
		wrong = true;
		this->m_textctrl->SetStyle(last_pos - 4, last_pos, style_wrong);
	}
	++p;

	this->m_textctrl->AppendText("\n");

	if (!wrong) *idx = p;
	return !wrong;
}

bool MyFrame::parse_graphic(const void * buf, size_t *idx, struct partDescriptor *part/* = NULL*/)
{
	bool wrong = false;
	auto style_wrong = this->m_textctrl->GetDefaultStyle();
	style_wrong.SetBackgroundColour(*wxRED);

	const unsigned char* d = (const unsigned char*) buf;
	size_t p = *idx;

	unsigned char type = d[p++];

	switch (type) {
		case 0x29:
			{
				long x0 = pick_long(d, &p);
				long y0 = pick_long(d, &p);
				long x1 = pick_long(d, &p);
				long y1 = pick_long(d, &p);

				int style = pick_long(d, &p);
				int width = pick_long(d, &p);

				wxString t = wxString::Format("G LINE: (%li,%li) -> (%li,%li), STYLE: %li, WIDTH: %li", x0, y0, x1, y1, style, width);
				this->m_textctrl->AppendText(t);
				this->m_textctrl->AppendText("\n");
				if (part) part->graphics.push_back(partGraphic{gLINE, {x0, y0, x1, y1, style, width}});
				*idx = p;
			}
			break;
		case 0x2a:
			{
				wxString t = "G 0x2A (ARC): ";
				long c1 = pick_long(d, &p);
				long c2 = pick_long(d, &p);
				long c3 = pick_long(d, &p);
				long c4 = pick_long(d, &p);
				long c5 = pick_long(d, &p);
				long c6 = pick_long(d, &p);
				long c7 = pick_long(d, &p);
				long c8 = pick_long(d, &p);

				int style = pick_long(d, &p);
				int width = pick_long(d, &p);

				t.Append(wxString::Format("((%li, %li)->(%li, %li) -- (%li, %li) -- (%li, %li)) STYLE: %li, WIDTH: %li", c1, c2, c3, c4, c5, c6, c7, c8, style, width));

				this->m_textctrl->AppendText(t);
				this->m_textctrl->AppendText("\n");
				*idx = p;
			}
			break;
		case 0x2b:
			{
				wxString t = "G 0x2B (ELLIPSE): ";
				long x1 = pick_long(d, &p);
				long y1 = pick_long(d, &p);
				long x2 = pick_long(d, &p);
				long y2 = pick_long(d, &p);
				long style = pick_long(d, &p);
				long width = pick_long(d, &p);
				long fill = pick_long(d, &p);
				long patt = pick_long(d, &p);
				t.Append(wxString::Format("(%li, %li) -> (%li, %li) STYLE: %li, WIDTH: %li, FILL: %li, PATT: %li", x1, y1, x2, y2, style, width, fill, patt));
				this->m_textctrl->AppendText(t);
				this->m_textctrl->AppendText("\n");
				*idx = p;
			}
			break;
		case 0x2c:
			{
				wxString t = "G 0x2C (POLY LINE): ";
				long style = pick_long(d, &p);
				long width = pick_long(d, &p);
				long fill = pick_long(d, &p);
				/* FILL:
				 * 0 -- solid
				 * 1 -- none
				 * 2 -- pattern
				 */
				long patt = pick_long(d, &p);
				int num_pt = pick_short(d, &p);
				t.Append(wxString::Format("STYLE: %li, WIDTH: %li, FILL: %li, PATT: %li, PTS: %i (", style, width, fill, patt, num_pt));
				for (int i = 0; i < num_pt; ++i) {
					short x = pick_short(d, &p);
					short y = pick_short(d, &p);
					t.Append(wxString::Format(" (%hi,%hi)", x, y));
				}
				t.Append(" )");
				this->m_textctrl->AppendText(t);
				this->m_textctrl->AppendText("\n");
				*idx = p;
			}
			break;
		case 0x2d:
			{
				wxString t = "G 0x2D (POLY LINE OPEN): ";
				long style = pick_long(d, &p);
				long width = pick_long(d, &p);
				int num_pt = pick_short(d, &p);
				t.Append(wxString::Format("STYLE: %li, WIDTH: %li, PTS: %i (", style, width, num_pt));
				for (int i = 0; i < num_pt; ++i) {
					short x = pick_short(d, &p);
					short y = pick_short(d, &p);
					t.Append(wxString::Format(" (%hi,%hi)", x, y));
				}
				t.Append(" )");
				this->m_textctrl->AppendText(t);
				this->m_textctrl->AppendText("\n");
				*idx = p;
			}
			break;
		case 0x57:
			{
				wxString t = "G 0x57 (BEZIER): ";
				long style = pick_long(d, &p);
				long width = pick_long(d, &p);
				int num_pt = pick_short(d, &p);
				t.Append(wxString::Format("STYLE: %li, WIDTH: %li, PTS: %i (", style, width, num_pt));
				for (int i = 0; i < num_pt; ++i) {
					short x = pick_short(d, &p);
					short y = pick_short(d, &p);
					t.Append(wxString::Format(" (%hi,%hi)", x, y));
				}
				t.Append(" )");
				this->m_textctrl->AppendText(t);
				this->m_textctrl->AppendText("\n");
				*idx = p;
			}
			break;
		case 0x28:
			{
				wxString t = "G 0x28 (RECT): ";
				long x1 = pick_long(d, &p);
				long y1 = pick_long(d, &p);
				long x2 = pick_long(d, &p);
				long y2 = pick_long(d, &p);
				long style = pick_long(d, &p);
				long width = pick_long(d, &p);
				long fill = pick_long(d, &p);
				long patt = pick_long(d, &p);
				t.Append(wxString::Format("(%li,%li) -> (%li, %li) STYLE: %li, WIDTH: %li, FILL: %li, PATT: %li", x1, y1, x2, y2, style, width, fill, patt));
				this->m_textctrl->AppendText(t);
				this->m_textctrl->AppendText("\n");
				*idx = p;
			}
			break;
		case 0x2e:
			{
				wxString t = "G 0x2e (TEXT): ";
				long c1 = pick_long(d, &p);
				long c2 = pick_long(d, &p);
				long c3 = pick_long(d, &p);
				long c4 = pick_long(d, &p);
				long c5 = pick_long(d, &p);
				long c6 = pick_long(d, &p);
				t.Append(wxString::Format("(%li, %li) (%li, %li) (%li, %li) //", c1, c2, c3, c4, c5, c6));

				for (size_t i = 0; i < 4; ++i) {
					t.Append(wxString::Format(" %.2x", (int)d[p++]));
				}
				int str_len = pick_short(d, &p);

				t.Append(wxString::Format(" (LEN = %04i): '", (int)str_len));

				for (int i = 0; i < str_len; ++i) {
					t.Append((char)d[p++], 1);
				}
				int zero = d[p++];
				t.Append(wxString::Format("' 0x%.2x", zero));
				if (zero) return false;

				/* Seems like LOGFONT structure */
				long fs = pick_long(d, &p);
				t.Append(wxString::Format(" FONT: %li //", fs));
				for (size_t i = 0; i < 24; ++i) {
					t.Append(wxString::Format(" %.2x", (int)d[p++]));
				}
				bool still_str = true;
				for (size_t i = 0; i < 32; ++i) {
					if (still_str && d[p] != 0) {
						if (i == 0) t.Append(" ");
						t.Append((char)d[p], 1);
					} else {
						if (still_str) {
							t.Append(" //");
							still_str = false;
						}
						t.Append(wxString::Format(" %.2x", (int)d[p]));
					}
					++p;
				}
				this->m_textctrl->AppendText(t);
				this->m_textctrl->AppendText("\n");
				*idx = p;
			}
			break;
		default:
			return false;
	}

	return !wrong;

}

bool MyFrame::parse_part(const void * buf, size_t *idx)
{
	const unsigned char *d = (const unsigned char *)buf;
	size_t p = *idx;


	this->m_textctrl->AppendText("============== PART (0x0d) =============\n");

	try {
		struct partDescriptor part;
		part.REF.name = "Part Reference";
		part.VAL.name = "Value";
		std::list<struct partText *> texts;
		texts.push_back(&part.REF);
		texts.push_back(&part.VAL);

		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

		while (d[p] == 0x18) {
			if (!this->parse_rec18(d, &p)) throw (unsigned long)__LINE__;

			/* different from here */
			this->parse_bytes(d, &p, 2, __LINE__);

			if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

			this->parse_bytes(d, &p, 1, __LINE__);

			if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
			if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
			if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

			this->parse_bytes(d, &p, 2, __LINE__);

			if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
			if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		}

		if (!this->parse_const(d, &p, (const unsigned char *)("\x1F"), 1)) throw (unsigned long)__LINE__;

		if (!this->parse_name_value_pairs(d, &p)) throw (unsigned long)__LINE__;

		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		this->m_textctrl->AppendText("STRING 980 ");
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

		this->parse_bytes(d, &p, 3, __LINE__);

		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

		{
			int num_rec = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("%.4x: NUMRECS: %i\n", (int)p, num_rec));
			for (int i = 0; i < num_rec; ++i) {
				if (!this->parse_rec20(d, &p, "\t")) throw (unsigned long)__LINE__;
			}
		}

		this->parse_bytes(d, &p, 1, __LINE__);

		if (!this->parse_name_value_pairs(d, &p)) throw (unsigned long)__LINE__;

		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

		this->parse_bytes(d, &p, 4, __LINE__);

		{
			part.pos.read(d, &p);
			this->m_textctrl->AppendText(wxString::Format("%s\n", part.pos.str()));
		}

		this->parse_bytes(d, &p, 1, __LINE__);

		{
			int rot = d[p++];
			this->m_textctrl->AppendText(wxString::Format("ROTATION: %i\n", rot));
			part.rot = rot;
		}

		this->parse_bytes(d, &p, 2, __LINE__);

		{
			int num_rec = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("NUMRECS: %i\n", num_rec));
			for (int i = 0; i < num_rec; ++i) {
				if (!this->parse_rec27(d, &p, "\t", &texts)) throw (unsigned long)__LINE__;
			}
		}

		if (!this->parse_const(d, &p, (const unsigned char *)("\x18"), 1)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

		if (!this->parse_rec18(d, &p, NULL, &part)) throw (unsigned long)__LINE__;

		this->parse_bytes(d, &p, 2, __LINE__);

		if (!this->parse_string(d, &p, &part.REF.value)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p, &part.VAL.value)) throw (unsigned long)__LINE__;

		this->parse_bytes(d, &p, 8, __LINE__);

		{
			int num_rec = d[p];
			num_rec |= (int)d[p + 1] << 8;
			this->m_textctrl->AppendText(wxString::Format("%.4x: NUMRECS: %i\n", (int)p, num_rec));
			p += 2;
			for (int i = 0; i < num_rec; ++i) {
				this->m_textctrl->AppendText(wxString::Format("\tREC %i\n", i));
				this->m_textctrl->AppendText("\t\t");
				if (!this->parse_const(d, &p, (const unsigned char *)("\x10\x00\x00"), 3)) throw (unsigned long)__LINE__;
				this->m_textctrl->AppendText("\t\t");
				if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
				this->m_textctrl->AppendText("\t\t");
				{
					size_t nb = 2;
					wxString line = wxString::Format("BYTES: (%05i)", __LINE__);
					for (size_t j = 0; j < nb; ++j) {
						line.Append(wxString::Format(" %02x", (int)d[p + j]));
					}
					line.Append("\n");
					this->m_textctrl->AppendText(line);
					p += nb;
				}
				this->m_textctrl->AppendText("\t\t");
				{
					short x = pick_short(d, &p);
					short y = pick_short(d, &p);
					this->m_textctrl->AppendText(wxString::Format("PIN COORD: (%hi, %hi)\n", x, y));
				}
				this->m_textctrl->AppendText("\t\t");
				this->parse_bytes(d, &p, 8, __LINE__);

				int flag = d[p++];
				this->m_textctrl->AppendText("\t\t");
				this->parse_bytes(d, &p, 1, __LINE__);
				this->m_textctrl->AppendText(wxString::Format("\t\tFLAG: 0x%.2x\n", flag));
				if (flag == 1) {
					if (!this->parse_rec27(d, &p, "\t\t\t")) throw (unsigned long)__LINE__;
				}

			}
		}

		this->parse_bytes(d, &p, 3, __LINE__);
		this->m_drawPanel->setPart(part);
		this->m_drawPanel->redraw();

	} catch (unsigned long l) {
		this->m_textctrl->AppendText(wxString::Format("!!!PARSE ERROR at line %li\n", l));
		*idx = p;
		return false;
	}
	*idx = p;
	return true;
}

bool MyFrame::parse_rec18(const void * buf, size_t *idx, const char * prefix, struct partDescriptor *part/* = NULL*/)
{
	/* ----------- START 0x18 ---------- */
	/*
	 * 0x18
	 * unsigned short fieldnum
	 * (string, string) x feldnum
	 * const
	 * string
	 * const
	 * unsigned short grnum
	 * (graphic rec) x grnum
	 * const
	 * unsigned short
	 * unsigned short
	 * const
	 * unsigned short numpins
	 * (pin rec) x numpins
	 * unsigned short numtxt
	 * (txt rec) x numtxt
	 * const
	 * str
	 * str
	 *
	 *
	 *
	 * bytes
	 * str
	 * byte
	 * const
	 * str
	 * str
	 * bytes
	 * str
	 * str
	 */
	const unsigned char *d = (const unsigned char *)buf;
	size_t p = *idx;

	wxString pr = prefix ? prefix : "";
	this->m_textctrl->AppendText(wxString::Format("%sREC 0x18:\n", pr));
	pr.Append("\t");

	try {
		this->m_textctrl->AppendText(pr);
		if (!this->parse_const(d, &p, (const unsigned char *)("\x18"), 1)) throw (unsigned long)__LINE__;
		this->m_textctrl->AppendText(pr);
		if (!this->parse_name_value_pairs(d, &p, pr.c_str())) throw (unsigned long)__LINE__;

		this->m_textctrl->AppendText(pr);
		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
		this->m_textctrl->AppendText(pr);
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		this->m_textctrl->AppendText(pr);
		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\x7F\x03\x00\x02\x00\x30\x00\x00\x00"), 10)) throw (unsigned long)__LINE__;

		{
			int num_gr = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("%sNUMGR: %i\n", pr, num_gr));

			for (int i = 0; i < num_gr; ++i) {
				this->m_textctrl->AppendText(wxString::Format("%s\t", pr));
				if (!this->parse_graphic(d, &p, part)) throw (unsigned long)__LINE__;
			}
		}

		this->m_textctrl->AppendText(pr);
		if (!this->parse_const(d, &p, (const unsigned char *)("\x00\x00\x00\x00"), 4)) throw (unsigned long)__LINE__;

		{
			struct partBB bb;
			bb.read(d, &p);
			this->m_textctrl->AppendText(wxString::Format("%s%s\n", pr, bb.str()));
			if (part) part->bb = bb;
		}

		this->m_textctrl->AppendText(pr);
		if (!this->parse_const(d, &p, (const unsigned char *)("\xff\x7f\x03\x00\x02\x00"), 6)) throw (unsigned long)__LINE__;

		{
			int num_pins = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("%sNUMPINS: %i\n", pr, num_pins));
			for (int i = 0; i < num_pins; ++i) {
				this->m_textctrl->AppendText(wxString::Format("%s\tPIN %i\n", pr, i));
				this->m_textctrl->AppendText(wxString::Format("%s\t", pr));
				if (!this->parse_const(d, &p, (const unsigned char *)("\x1a"), 1)) throw (unsigned long)__LINE__;

				if (!this->parse_name_value_pairs(d, &p, wxString::Format("%s\t", pr).c_str())) throw (unsigned long)__LINE__;

				this->m_textctrl->AppendText(wxString::Format("%s\t", pr));
				if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
				this->m_textctrl->AppendText(wxString::Format("%s\t", pr));
				if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\x7F\x03\x00\x02\x00"), 6)) throw (unsigned long)__LINE__;
				this->m_textctrl->AppendText(wxString::Format("%s\t", pr));
				if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

				long x1 = pick_long(d, &p);
				long y1 = pick_long(d, &p);
				long x2 = pick_long(d, &p);
				long y2 = pick_long(d, &p);
				this->m_textctrl->AppendText(wxString::Format("%s\tPOS: (%li, %li) -> (%li, %li)\n", pr, x1, y1, x2, y2));
				if (part) part->graphics.push_back(partGraphic{gPIN, {x1, y1, x2, y2}});

				this->m_textctrl->AppendText(wxString::Format("%s\t", pr));
				this->parse_bytes(d, &p, 14, __LINE__);
			}
		}

		/* 
		 * 0x27 normal field
		 * 0xf8 - text with font spec
		 */
		{
			int num_rec = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("%s%.4x: NUMRECS: %i\n", pr, (int)p, num_rec));
			for (int i = 0; i < num_rec; ++i) {
				if (!this->parse_rec27(d, &p, wxString::Format("%s\t", pr).c_str())) throw (unsigned long)__LINE__;
			}
		}

		this->m_textctrl->AppendText(pr);
		if (!this->parse_const(d, &p, (const unsigned char *)("\x00\x00\x00\x00\x00\x00"), 6)) throw (unsigned long)__LINE__;

		this->m_textctrl->AppendText(pr);
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		this->m_textctrl->AppendText(pr);
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

	} catch (unsigned long l) {
		this->m_textctrl->AppendText(wxString::Format("!!!PARSE ERROR at line %li\n", l));
		*idx = p;
		return false;
	}
	*idx = p;
	return true;
}


bool MyFrame::parse_wire(const void * buf, size_t *idx)
{
	const unsigned char *d = (const unsigned char *)buf;
	size_t p = *idx;


	this->m_textctrl->AppendText("============== WIRE (0x14) =============\n");

	try {
		if (!this->parse_name_value_pairs(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;

		this->parse_bytes(d, &p, 8, __LINE__, (const unsigned char *)("\x8f\x2e\x00\x01\x30\x00\x00\x00"));

		{
			long y1 = pick_long(d, &p);
			long x1 = pick_long(d, &p);
			long y2 = pick_long(d, &p);
			long x2 = pick_long(d, &p);
			this->m_textctrl->AppendText(wxString::Format("COORDS: (%li, %li, %li, %li)\n", y1, x1, y2, x2));
		}

		this->parse_bytes(d, &p, 1, __LINE__, (const unsigned char *)("\x00"));

		unsigned char named  = d[p++];

		this->m_textctrl->AppendText(named ? "NAMED\n" : "UNNAMED\n");

		this->parse_bytes(d, &p, 12, __LINE__, (const unsigned char *)("\x00\x00\x00\x03\x00\x00\x00\x05\x00\x00\x00\x00"));

		if (named) {
			this->parse_bytes(d, &p, 14, __LINE__, (const unsigned char *)("\x00\x00\xee\x02\x00\x00\x30\x00\x00\x00\x00\x00\x00\x00"));

			int flag = d[p++];
			this->m_textctrl->AppendText(wxString::Format("FLAG: 0x%.2x\n", (int)flag));

			this->parse_bytes(d, &p, 3, __LINE__, (const unsigned char *)("\x00\x00\x00"));

			if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

			if (flag) {
				this->parse_bytes(d, &p, 60, __LINE__);
			}

			this->parse_bytes(d, &p, 11, __LINE__);
		}

	} catch (unsigned long l) {
		this->m_textctrl->AppendText(wxString::Format("!!!PARSE ERROR at line %li\n", l));
		*idx = p;
		return false;
	}
	*idx = p;
	return true;
}

bool MyFrame::parse_power(const void * buf, size_t *idx)
{
	const unsigned char *d = (const unsigned char *)buf;
	size_t p = *idx;


	this->m_textctrl->AppendText("============== POWER (0x25) =============\n");

	try {
		if (!this->parse_name_value_pairs(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

		{
			size_t nb = 20;
			wxString line = wxString::Format("BYTES: (%05i)", (int)__LINE__);
			for (size_t j = 0; j < nb; ++j) {
				line.Append(wxString::Format(" %02x", (int)d[p + j]));
			}
			line.Append("\n");
			this->m_textctrl->AppendText(line);
			p += nb;
		}

		{
			int num_rec = d[p];
			num_rec |= (int)d[p + 1] << 8;
			this->m_textctrl->AppendText(wxString::Format("%.4x: NUMRECS: %i\n", (int)p, num_rec));
			p += 2;
			for (int i = 0; i < num_rec; ++i) {
				if (!this->parse_rec27(d, &p, "\t")) throw (unsigned long)__LINE__;
			}
		}

		if (!this->parse_const(d, &p, (const unsigned char *)("\x21"), 1)) throw (unsigned long)__LINE__;

		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

		if (!this->parse_const(d, &p, (const unsigned char *)("\x21"), 1)) throw (unsigned long)__LINE__;

		{
			int num_str = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("NUMSTRS: (%05i) %i\n", __LINE__, num_str));

			for (int i = 0; i < num_str * 2; ++i) {
				this->m_textctrl->AppendText("\t");
				if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
			}
		}

		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\x7F\x03\x00\x02\x00"), 6)) throw (unsigned long)__LINE__;

		if (!this->parse_const(d, &p, (const unsigned char *)("\x30\x00\x00\x00"), 4)) throw (unsigned long)__LINE__;

		{
			int num_gr = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("NUMGR: %i\n", num_gr));

			for (int i = 0; i < num_gr; ++i) {
				this->m_textctrl->AppendText("\t");
				if (!this->parse_graphic(d, &p)) throw (unsigned long)__LINE__;
			}
		}

		if (!this->parse_const(d, &p, (const unsigned char *)("\x00\x00\x00\x00"), 4)) throw (unsigned long)__LINE__;

		{
			size_t nb = 4;
			wxString line = wxString::Format("BYTES: (%05i)", (int)__LINE__);
			for (size_t j = 0; j < nb; ++j) {
				line.Append(wxString::Format(" %02x", (int)d[p + j]));
			}
			line.Append("\n");
			this->m_textctrl->AppendText(line);
			p += nb;
		}

		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\x7F\x03\x00\x02\x00"), 6)) throw (unsigned long)__LINE__;

		{
			int num_pins = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("NUMPINS: %i\n", num_pins));
			for (int i = 0; i < num_pins; ++i) {
				this->m_textctrl->AppendText(wxString::Format("\tPIN %i\n", i));
				this->m_textctrl->AppendText("\t");
				if (!this->parse_const(d, &p, (const unsigned char *)("\x1a"), 1)) throw (unsigned long)__LINE__;
				int num_str = pick_short(d, &p);
				this->m_textctrl->AppendText(wxString::Format("\tNUMSTRS: %i\n", num_str));

				for (int i = 0; i < num_str * 2; ++i) {
					this->m_textctrl->AppendText("\\t");
					if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
				}

				this->m_textctrl->AppendText("\t");
				if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
				this->m_textctrl->AppendText("\t");
				if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\x7F\x03\x00\x02\x00"), 6)) throw (unsigned long)__LINE__;
				this->m_textctrl->AppendText("\t");
				if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

				long x1 = pick_long(d, &p);
				long y1 = pick_long(d, &p);
				long x2 = pick_long(d, &p);
				long y2 = pick_long(d, &p);
				this->m_textctrl->AppendText(wxString::Format("\tPOS: (%li, %li) -> (%li, %li)\n", x1, y1, x2, y2));

				wxString line = "\tBYTES:";
				for (size_t j = 0; j < 14; ++j) {
					line.Append(wxString::Format(" %02x", (int)d[p + j]));
				}
				line.Append("\n");
				this->m_textctrl->AppendText(line);
				p += 14;
			}
		}

		/* 
		 * 0x27 normal field
		 * 0xf8 - text with font spec
		 */
		{
			int num_rec = d[p];
			num_rec |= (int)d[p + 1] << 8;
			this->m_textctrl->AppendText(wxString::Format("%.4x: NUMRECS: %i\n", (int)p, num_rec));
			p += 2;
			for (int i = 0; i < num_rec; ++i) {
				if (!this->parse_rec27(d, &p, "\t")) throw (unsigned long)__LINE__;
			}
		}

		{
			size_t nb = 6;
			wxString line = wxString::Format("BYTES: (%05i)", (int)__LINE__);
			for (size_t j = 0; j < nb; ++j) {
				line.Append(wxString::Format(" %02x", (int)d[p + j]));
			}
			line.Append("\n");
			this->m_textctrl->AppendText(line);
			p += nb;
		}

	} catch (unsigned long l) {
		this->m_textctrl->AppendText(wxString::Format("!!!PARSE ERROR at line %li\n", l));
		*idx = p;
		return false;
	}
	*idx = p;
	return true;
}

bool MyFrame::parse_offpage(const void * buf, size_t *idx)
{
	const unsigned char *d = (const unsigned char *)buf;
	size_t p = *idx;


	this->m_textctrl->AppendText("============== OFFPAGE (0x26) =============\n");

	try {
		if (!this->parse_name_value_pairs(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

		{
			size_t nb = 20;
			wxString line = wxString::Format("BYTES: (%05i)", (int)__LINE__);
			for (size_t j = 0; j < nb; ++j) {
				line.Append(wxString::Format(" %02x", (int)d[p + j]));
			}
			line.Append("\n");
			this->m_textctrl->AppendText(line);
			p += nb;
		}

		{
			int num_rec = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("%.4x: NUMRECS: %i\n", (int)p, num_rec));
			for (int i = 0; i < num_rec; ++i) {
				if (!this->parse_rec27(d, &p, "\t")) throw (unsigned long)__LINE__;
			}
		}

		if (!this->parse_const(d, &p, (const unsigned char *)("\x23"), 1)) throw (unsigned long)__LINE__;

		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

		if (!this->parse_const(d, &p, (const unsigned char *)("\x23"), 1)) throw (unsigned long)__LINE__;

		if (!this->parse_name_value_pairs(d, &p)) throw (unsigned long)__LINE__;

		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\x7F\x03\x00\x02\x00"), 6)) throw (unsigned long)__LINE__;

		if (!this->parse_const(d, &p, (const unsigned char *)("\x30\x00\x00\x00"), 4)) throw (unsigned long)__LINE__;

		{
			int num_gr = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("NUMGR: %i\n", num_gr));

			for (int i = 0; i < num_gr; ++i) {
				this->m_textctrl->AppendText("\t");
				if (!this->parse_graphic(d, &p)) throw (unsigned long)__LINE__;
			}
		}

		if (!this->parse_const(d, &p, (const unsigned char *)("\x00\x00\x00\x00"), 4)) throw (unsigned long)__LINE__;

		{
			size_t nb = 4;
			wxString line = wxString::Format("BYTES: (%05i)", (int)__LINE__);
			for (size_t j = 0; j < nb; ++j) {
				line.Append(wxString::Format(" %02x", (int)d[p + j]));
			}
			line.Append("\n");
			this->m_textctrl->AppendText(line);
			p += nb;
		}

		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\x7F\x03\x00\x02\x00"), 6)) throw (unsigned long)__LINE__;

		{
			int num_pins = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("NUMPINS: %i\n", num_pins));
			for (int i = 0; i < num_pins; ++i) {
				this->m_textctrl->AppendText(wxString::Format("\tPIN %i\n", i));
				this->m_textctrl->AppendText("\t");
				if (!this->parse_const(d, &p, (const unsigned char *)("\x1a"), 1)) throw (unsigned long)__LINE__;
				int num_str = pick_short(d, &p);
				this->m_textctrl->AppendText(wxString::Format("\tNUMSTRS: %i\n", num_str));

				for (int i = 0; i < num_str * 2; ++i) {
					this->m_textctrl->AppendText("\\t");
					if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
				}

				this->m_textctrl->AppendText("\t");
				if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
				this->m_textctrl->AppendText("\t");
				if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\x7F\x03\x00\x02\x00"), 6)) throw (unsigned long)__LINE__;
				this->m_textctrl->AppendText("\t");
				if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

				long x1 = pick_long(d, &p);
				long y1 = pick_long(d, &p);
				long x2 = pick_long(d, &p);
				long y2 = pick_long(d, &p);
				this->m_textctrl->AppendText(wxString::Format("\tPOS: (%li, %li) -> (%li, %li)\n", x1, y1, x2, y2));

				wxString line = "\tBYTES:";
				for (size_t j = 0; j < 14; ++j) {
					line.Append(wxString::Format(" %02x", (int)d[p + j]));
				}
				line.Append("\n");
				this->m_textctrl->AppendText(line);
				p += 14;
			}
		}

		/* 
		 * 0x27 normal field
		 * 0xf8 - text with font spec
		 */
		{
			int num_rec = d[p];
			num_rec |= (int)d[p + 1] << 8;
			this->m_textctrl->AppendText(wxString::Format("%.4x: NUMRECS: %i\n", (int)p, num_rec));
			p += 2;
			for (int i = 0; i < num_rec; ++i) {
				if (!this->parse_rec27(d, &p, "\t")) throw (unsigned long)__LINE__;
			}
		}

		{
			size_t nb = 6;
			wxString line = wxString::Format("BYTES: (%05i)", (int)__LINE__);
			for (size_t j = 0; j < nb; ++j) {
				line.Append(wxString::Format(" %02x", (int)d[p + j]));
			}
			line.Append("\n");
			this->m_textctrl->AppendText(line);
			p += nb;
		}

	} catch (unsigned long l) {
		this->m_textctrl->AppendText(wxString::Format("!!!PARSE ERROR at line %li\n", l));
		*idx = p;
		return false;
	}
	*idx = p;
	return true;
}

bool MyFrame::parse_hier(const void * buf, size_t *idx)
{
	const unsigned char *d = (const unsigned char *)buf;
	size_t p = *idx;


	this->m_textctrl->AppendText("============== HIER (0x17) =============\n");

	try {
		if (!this->parse_name_value_pairs(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

		this->parse_bytes(d, &p, 4, __LINE__, (const unsigned char *)("\x51\x2c\x00\x01"));

		{
			short y1 = pick_short(d, &p);
			short x1 = pick_short(d, &p);
			short y2 = pick_short(d, &p);
			short x2 = pick_short(d, &p);
			short x3 = pick_short(d, &p);
			short y3 = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("COORDS: (%hi, %hi, %hi, %hi, %hi, %hi)\n", y1, x1, y2, x2, x3, y3));
		}

		this->parse_bytes(d, &p, 4, __LINE__, (const unsigned char *)("\x30\x00\x17\x00"));

		{
			int num_rec = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("%.4x: NUMRECS: %i\n", (int)p, num_rec));
			for (int i = 0; i < num_rec; ++i) {
				if (!this->parse_rec27(d, &p, "\t")) throw (unsigned long)__LINE__;
			}
		}

		if (!this->parse_const(d, &p, (const unsigned char *)("\x22"), 1)) throw (unsigned long)__LINE__;

		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

		if (!this->parse_const(d, &p, (const unsigned char *)("\x22"), 1)) throw (unsigned long)__LINE__;

		if (!this->parse_name_value_pairs(d, &p)) throw (unsigned long)__LINE__;

		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\x7F\x03\x00\x02\x00"), 6)) throw (unsigned long)__LINE__;

		if (!this->parse_const(d, &p, (const unsigned char *)("\x30\x00\x00\x00"), 4)) throw (unsigned long)__LINE__;

		{
			int num_gr = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("NUMGR: %i\n", num_gr));

			for (int i = 0; i < num_gr; ++i) {
				this->m_textctrl->AppendText("\t");
				if (!this->parse_graphic(d, &p)) throw (unsigned long)__LINE__;
			}
		}

		if (!this->parse_const(d, &p, (const unsigned char *)("\x00\x00\x00\x00"), 4)) throw (unsigned long)__LINE__;

		{
			short w = pick_short(d, &p);
			short h = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("BOUNDING RECT: %hi x %hi\n", w, h));
		}

		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\x7F\x03\x00\x02\x00"), 6)) throw (unsigned long)__LINE__;

		{
			int num_pins = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("NUMPINS: %i\n", num_pins));
			for (int i = 0; i < num_pins; ++i) {
				this->m_textctrl->AppendText(wxString::Format("\tPIN %i\n", i));
				this->m_textctrl->AppendText("\t");
				if (!this->parse_const(d, &p, (const unsigned char *)("\x1a"), 1)) throw (unsigned long)__LINE__;
				int num_str = pick_short(d, &p);
				this->m_textctrl->AppendText(wxString::Format("\tNUMSTRS: %i\n", num_str));

				for (int i = 0; i < num_str * 2; ++i) {
					this->m_textctrl->AppendText("\\t");
					if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
				}

				this->m_textctrl->AppendText("\t");
				if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
				this->m_textctrl->AppendText("\t");
				if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\x7F\x03\x00\x02\x00"), 6)) throw (unsigned long)__LINE__;
				this->m_textctrl->AppendText("\t");
				if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

				long x1 = pick_long(d, &p);
				long y1 = pick_long(d, &p);
				long x2 = pick_long(d, &p);
				long y2 = pick_long(d, &p);
				this->m_textctrl->AppendText(wxString::Format("\tPOS: (%li, %li) -> (%li, %li)\n", x1, y1, x2, y2));

				this->m_textctrl->AppendText("\t");
				this->parse_bytes(d, &p, 14, __LINE__);
			}
		}

		{
			int num_rec = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("NUMRECS: %i\n", num_rec));
			for (int i = 0; i < num_rec; ++i) {
				if (!this->parse_rec27(d, &p, "\t")) throw (unsigned long)__LINE__;
			}
		}

		this->parse_bytes(d, &p, 6, __LINE__);

		{
			/* TYPE:
			 * 0 - input
			 * 2 - output
			 * 3 - open collector
			 */
			int type = d[p++];
			this->m_textctrl->AppendText(wxString::Format("TYPE: %i\n", type));
		}

		this->parse_bytes(d, &p, 3, __LINE__);

	} catch (unsigned long l) {
		this->m_textctrl->AppendText(wxString::Format("!!!PARSE ERROR at line %li\n", l));
		*idx = p;
		return false;
	}
	*idx = p;
	return true;
}

bool MyFrame::parse_rec27(const void * buf, size_t *idx, const char * prefix, std::list<struct partText *> * texts /* = NULL*/)
{
	/* text description */
	const unsigned char *d = (const unsigned char *)buf;
	size_t p = *idx;

	wxString pr = prefix ? prefix : "";
	this->m_textctrl->AppendText(wxString::Format("%sREC 0x27:\n", pr));
	pr.Append("\t");

	try {
		this->m_textctrl->AppendText(pr);
		if (!this->parse_const(d, &p, (const unsigned char *)("\x27\xFF\xE4\x5C\x39\x00\x00\x00\x00\xff\x7f\x03\x00\x02\x00"), 15)) throw (unsigned long)__LINE__;
		this->m_textctrl->AppendText(pr);
		std::string name;
		if (!this->parse_string(d, &p, &name)) throw (unsigned long)__LINE__;

		short x1 = pick_short(d, &p);
		short y1 = pick_short(d, &p);
		this->m_textctrl->AppendText(wxString::Format("%sPOS: (%hi, %hi)\n", pr, x1, y1));

		int flag = d[p++];
		this->m_textctrl->AppendText(wxString::Format("%sFLAG: 0x%.2x\n", pr, flag));
		int rot = d[p++];
		this->m_textctrl->AppendText(wxString::Format("%sROT: 0x%.2x\n", pr, rot));
		int color = d[p++];
		this->m_textctrl->AppendText(wxString::Format("%sCOLOR: 0x%.2x\n", pr, color));
		this->m_textctrl->AppendText(pr);
		this->parse_bytes(d, &p, 1, __LINE__);
		/* Display type:
		 * 1 -- value only
		 * 2 -- name and value
		 */
		int disp_type = d[p++];
		this->m_textctrl->AppendText(wxString::Format("%sDISPLAY: 0x%.2x\n", pr, (int)disp_type));
		this->m_textctrl->AppendText(pr);
		this->parse_bytes(d, &p, 1, __LINE__, (const unsigned char *)("\x00"));


		if (flag) {
			/* TODO: LOGFONT */
			this->m_textctrl->AppendText(pr);
			this->parse_bytes(d, &p, 60, __LINE__);
		}

		if (texts) {
			for (auto t : *texts) {
				if (!t->name.compare(name)) {
					t->x = x1;
					t->y = y1;
					t->flag = flag;
					t->rot = rot;
					t->color = color;
					t->display = disp_type;
				}
			}
		}

	} catch (unsigned long l) {
		this->m_textctrl->AppendText(wxString::Format("!!!PARSE ERROR at line %li\n", l));
		*idx = p;
		return false;
	}
	*idx = p;
	return true;
}

bool MyFrame::parse_rec20(const void * buf, size_t *idx, const char * prefix)
{
	const unsigned char *d = (const unsigned char *)buf;
	size_t p = *idx;

	wxString pr = prefix ? prefix : "";
	this->m_textctrl->AppendText(wxString::Format("%sREC 0x20:\n", pr));
	pr.Append("\t");

	try {
		this->m_textctrl->AppendText(pr);
		if (!this->parse_const(d, &p, (const unsigned char *)("\x20\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 9)) throw (unsigned long)__LINE__;
		this->m_textctrl->AppendText(pr);
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		this->m_textctrl->AppendText(pr);
		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

		{
			int num_pins = pick_short(d, &p);
			this->m_textctrl->AppendText(wxString::Format("%sNUMPINS: %i\n", pr, num_pins));
			for (int i = 0; i < num_pins; ++i) {
				wxString prr = pr;
				prr.Append("\t");
				this->m_textctrl->AppendText(wxString::Format("%sPIN %i\n", prr, i));
				this->m_textctrl->AppendText(prr);
				if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
				this->m_textctrl->AppendText(prr);
				this->parse_bytes(d, &p, 1, __LINE__, (const unsigned char *)("\x7f"));
			}
		}

	} catch (unsigned long l) {
		this->m_textctrl->AppendText(wxString::Format("!!!PARSE ERROR at line %li\n", l));
		*idx = p;
		return false;
	}
	*idx = p;
	return true;
}

bool MyFrame::parse_text(const void * buf, size_t *idx)
{
	const unsigned char *d = (const unsigned char *)buf;
	size_t p = *idx;


	this->m_textctrl->AppendText("============== TEXT (0x3d) =============\n");

	try {
		if (!this->parse_const(d, &p, (const unsigned char *)("\x00\x00"), 2)) throw (unsigned long)__LINE__;
		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;

		if (!this->parse_const(d, &p, (const unsigned char *)("\x00\x00\x00\x00\x00\x00\x00\x00\x00"), 9)) throw (unsigned long)__LINE__;
		this->parse_bytes(d, &p, 3, __LINE__);
		if (!this->parse_const(d, &p, (const unsigned char *)("\x00\x00\x00\x00\x00\x00\x00\x00\x00"), 9)) throw (unsigned long)__LINE__;
		short rx1 = pick_short(d, &p);
		short ry1 = pick_short(d, &p);
		this->m_textctrl->AppendText(wxString::Format("RELATIVE MOVE??: (%hi, %hi)\n", rx1, ry1));
		this->parse_bytes(d, &p, 10, __LINE__, (const unsigned char *)("\x24\x00\x3d\x00\x00\x00\x02\x02\x00\x00"));
		if (!this->parse_const(d, &p, (const unsigned char *)("\xff\xe4\x5c\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;
		if (!this->parse_const(d, &p, (const unsigned char *)("\x00\x00\x00\xff\x7f\x03\x00\x02\x00"), 9)) throw (unsigned long)__LINE__;
		if (!this->parse_const(d, &p, (const unsigned char *)("\x30\x00\x00\x00\x01\x00\x2e"), 7)) throw (unsigned long)__LINE__;

		long x1 = pick_long(d, &p);
		long y1 = pick_long(d, &p);
		long x2 = pick_long(d, &p);
		long y2 = pick_long(d, &p);
		long x3 = pick_long(d, &p);
		long y3 = pick_long(d, &p);
		this->m_textctrl->AppendText(wxString::Format("POS: (%li, %li) -> (%li, %li) -> (%li, %li)\n", x1, y1, x2, y2, x3, y3));

		int flag = d[p++];
		this->m_textctrl->AppendText(wxString::Format("FLAG: 0x%.2x\n", (int)flag));
		this->parse_bytes(d, &p, 3, __LINE__, (const unsigned char *)("\x00\x00\x00"));

		if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;

		if (flag) {
			size_t nb = 60;
			wxString line = wxString::Format("BYTES: (%05i)", (int)__LINE__);
			for (size_t j = 0; j < nb; ++j) {
				line.Append(wxString::Format(" %02x", (int)d[p + j]));
			}
			line.Append("\n");
			this->m_textctrl->AppendText(line);
			p += nb;
		}

		this->parse_bytes(d, &p, 8, __LINE__, (const unsigned char *)("\x00\x00\x00\x00\x00\x00\x00\x00"));

	} catch (unsigned long l) {
		this->m_textctrl->AppendText(wxString::Format("!!!PARSE ERROR at line %li\n", l));
		*idx = p;
		return false;
	}
	*idx = p;
	return true;
}

bool MyFrame::parse_line(const void * buf, size_t *idx)
{
	const unsigned char *d = (const unsigned char *)buf;
	size_t p = *idx;


	this->m_textctrl->AppendText("============== LINE (0x3d) =============\n");

	try {
		if (!this->parse_const(d, &p, (const unsigned char *)("\x00\x00"), 2)) throw (unsigned long)__LINE__;
		if (!this->parse_const(d, &p, (const unsigned char *)("\xFF\xE4\x5C\x39\x00\x00\x00\x00"), 8)) throw (unsigned long)__LINE__;

		{
			size_t nb = 9 * 16 - 5 - 2 - 8 - 38;
			wxString line = wxString::Format("BYTES: (%5i)", (int)__LINE__);
			for (size_t j = 0; j < nb; ++j) {
				if (j != 0 && j % 16 == 0) {
					line.Append("\n              ");
				}
				line.Append(wxString::Format(" %02x", (int)d[p + j]));
			}
			line.Append("\n");
			this->m_textctrl->AppendText(line);
			p += nb;
		}

	} catch (unsigned long l) {
		this->m_textctrl->AppendText(wxString::Format("!!!PARSE ERROR at line %li\n", l));
		*idx = p;
		return false;
	}
	*idx = p;
	return true;
}

bool MyFrame::parse_name_value_pairs(const void * buf, size_t *idx, const char * prefix)
{
	const unsigned char *d = (const unsigned char *)buf;
	size_t p = *idx;

	int num_pairs = pick_short(d, &p);
	wxString pr = prefix ? prefix : "";
	this->m_textctrl->AppendText(wxString::Format("%sNAME->VALUE PAIRS (%5i)%s\n", pr, num_pairs, num_pairs ? ":" : ""));
	pr.Append("\t");

	try {
		for (int i = 0; i < num_pairs * 2; ++i) {
			this->m_textctrl->AppendText(pr);
			if (!this->parse_string(d, &p)) throw (unsigned long)__LINE__;
		}

	} catch (unsigned long l) {
		this->m_textctrl->AppendText(wxString::Format("!!!PARSE ERROR at line %li\n", l));
		*idx = p;
		return false;
	}
	*idx = p;
	return true;
}

void MyFrame::OnReplace(wxCommandEvent& WXUNUSED(event))
{
	this->replace();
}


#if USE_ASYNCHRONOUS_CLIPBOARD_REQUEST
void MyFrame::OnClipboardChange(wxClipboardEvent&event)
{
    m_clipboardSupportsText = event.SupportsFormat( wxDF_UNICODETEXT );
    m_request = Finished;
}
#endif

void MyFrame::OnUpdateUIWrite(wxUpdateUIEvent&event)
{
#if USE_ASYNCHRONOUS_CLIPBOARD_REQUEST
    if (m_request == Idle)
    {
        if (!wxTheClipboard->IsSupportedAsync( this ))
        {
            // request failed, try again later
            event.Enable( m_clipboardSupportsText ); // not yet known, assume last value
            return;
        }
        m_request = Waiting;
        event.Enable( m_clipboardSupportsText ); // not yet known, assume last value
    }
    else if (m_request == Waiting)
    {
        event.Enable( m_clipboardSupportsText ); // not yet known, assume last value
    }
    else if (m_request == Finished)
    {
        event.Enable( m_clipboardSupportsText );
        m_request = Idle;
    }
#else
    event.Enable( wxTheClipboard->IsSupported( wxDF_UNICODETEXT ) );
#endif
}

void MyFrame::OnUpdateUICmp(wxUpdateUIEvent&event)
{
    event.Enable( wxTheClipboard->IsSupported( this->m_orcadFormat ) );
}

void MyFrame::OnUpdateUIAnalyzeData(wxUpdateUIEvent&event)
{
    event.Enable( wxTheClipboard->IsSupported( this->m_orcadFormat ) );
}

bool MyFrame::isCLERPheader(const wxString &s, size_t * positions)
{
	auto vec = tokenize(s.ToStdString(), '\t');
	int lpos[32];
	static const char * hdr_fields_parts[] = {
		" Part PN ",
		" Part Name ",
		" Land Pattern ",
		" Part Status ",
		" Family ",
		" Testing Required ",
		" Critical Part ",
		" Display Unit ",
		" Quantity ",
		" Budget Category ",
		" PO To Stock ",
	};

	if (vec.size() == sizeof(hdr_fields_parts) / sizeof(const char *)) {
		bool match = true;
		for (size_t i = 0; i < sizeof(hdr_fields_parts) / sizeof(const char *); ++i) {
			lpos[i] = -1;
		}

		for (size_t i = 0; i < sizeof(hdr_fields_parts) / sizeof(const char *); ++i) {
			bool found = false;
			for (size_t j = 0; j < vec.size(); ++j) {
				if (!vec[j].compare(hdr_fields_parts[i])) {
					if (lpos[i] >= 0) {
						match = false;
						break;
					}
					lpos[i] = j;
					found = true;
				}
			}
			if (! match) break;
			if (! found) {
				match = false;
				break;
			}
		}

		if (match) {
			for (size_t i = 0; i < sizeof(hdr_fields_parts) / sizeof(const char *); ++i) {
				if (lpos[i] < 0) {
					match = false;
					break;
				}
			}
		}

		if (match && positions) {
			positions[0] = lpos[0];
			positions[1] = lpos[1];
			positions[2] = lpos[2];
			positions[3] = lpos[3];
			positions[4] = lpos[4];
		}
		if (match) return true;
	}

	static const char * hdr_fields_add_part[] = {
		"  ",
		" Part PN ",
		" Part name ",
		" Land Pattern ",
		" Part Status ",
		" Comments ",
		" Family ",
		" Display Unit ",
		" Testing Required ",
		" User ",
		" Budget Category "
	};

	if (vec.size() == sizeof(hdr_fields_add_part) / sizeof(const char *)) {
		bool match = true;
		for (size_t i = 0; i < sizeof(hdr_fields_add_part) / sizeof(const char *); ++i) {
			lpos[i] = -1;
		}

		for (size_t i = 0; i < sizeof(hdr_fields_add_part) / sizeof(const char *); ++i) {
			bool found = false;
			for (size_t j = 0; j < vec.size(); ++j) {
				if (!vec[j].compare(hdr_fields_add_part[i])) {
					if (lpos[i] >= 0) {
						match = false;
						break;
					}
					lpos[i] = j;
					found = true;
				}
			}
			if (! match) break;
			if (! found) {
				match = false;
				break;
			}
		}

		if (match) {
			for (size_t i = 0; i < sizeof(hdr_fields_add_part) / sizeof(const char *); ++i) {
				if (lpos[i] < 0) {
					match = false;
					break;
				}
			}
		}

		if (match && positions) {
			positions[0] = lpos[1];
			positions[1] = lpos[2];
			positions[2] = lpos[3];
			positions[3] = lpos[4];
			positions[4] = lpos[6];
		}
		if (match) return true;
	}

	return false;
}

void MyFrame::OnUpdateUIReplace(wxUpdateUIEvent&event)
{
	if (!wxTheClipboard->IsSupported( wxDF_UNICODETEXT ) ) {
		event.Enable(false);
		return;
	}
	if (wxTheClipboard->Open())
	{
		if (wxTheClipboard->IsSupported( wxDF_UNICODETEXT ))
		{
			wxTextDataObject data;
			wxTheClipboard->GetData( data );
			wxString text = data.GetText();
			if (this->isCLERPheader(text.BeforeFirst('\n'))) {
				event.Enable(true);
			} else {
				event.Enable(false);
			}

		} else {
			event.Enable(false);
		}
		wxTheClipboard->Close();
	}
}

void MyFrame::OnClipboardChange()
{
	if (this->m_autoReplace) {
		this->replace();
	}
}

void MyFrame::makeValue()
{
	wxConfigBase *cfg = wxConfigBase::Get();
	std::string sep = cfg->Read("/components/value-separator", ",").ToStdString();
	if (!this->m_curPart.type.compare("res")) {
		std::string v = this->m_curPart.vfields["VAL"];
		for (const auto &f : this->m_resVFnames) {
			if (this->m_curPart.vfields.count(f) && !this->m_curPart.vfields.at(f).empty() && this->m_resVFtgl.at(f)->GetValue()) {
				v = v + sep + this->m_curPart.vfields.at(f);
			}
		}
		this->m_curPart.VAL.value = v;
	} else if (!this->m_curPart.type.compare("cnp")) {
		std::string v = this->m_curPart.vfields["VAL"];
		for (const auto &f : this->m_cnpVFnames) {
			if (this->m_curPart.vfields.count(f) && !this->m_curPart.vfields.at(f).empty() && this->m_cnpVFtgl.at(f)->GetValue()) {
				v = v + sep + this->m_curPart.vfields.at(f);
			}
		}
		this->m_curPart.VAL.value = v;
	}
}

void MyFrame::replace()
{
	wxConfigBase *cfg = wxConfigBase::Get();

	if (!wxTheClipboard->Open()) return;

	if (!wxTheClipboard->IsSupported( wxDF_UNICODETEXT )) {
		wxTheClipboard->Close();
		return;
	}

	wxTextDataObject data;
	wxTheClipboard->GetData( data );
	wxString text = data.GetText();

	size_t positions[8];

	if (!this->isCLERPheader(text.BeforeFirst('\n'), positions)) {
		wxTheClipboard->Close();
		return;
	}

	text = text.AfterFirst('\n').BeforeFirst('\n');

	auto fvec = tokenize(text.BeforeFirst('\n').ToStdString(), '\t');

	std::map<std::string, std::string> fmap;

	wxString pn = fvec[positions[0]];
	wxString fp = fvec[positions[2]];

	if (!(fmap = this->getRes(fvec, positions)).empty()) {
		this->m_curPart = resistorDescriptor;

		this->m_curPart.vfields = fmap;
		for (const auto & f : this->m_resVFtxt) f.second->SetValue(fmap.at(f.first));
		for (const auto & f : this->m_cnpVFtxt) f.second->SetValue("");
		this->makeValue();

		this->m_curPart.lib = this->getDISCRETE_OLB().Upper().ToStdString();
		this->m_curPart.fields["P/N"] = fmap["P/N"];
		this->m_curPart.fields["Part Name"] = fmap["Part Name"];
		this->m_curPart.fields["PCB Footprint"] = fmap["PCB Footprint"];
		for (const auto & f : this->m_userProperties) {
			if (std::get<0>(f)->GetValue() && std::get<1>(f)->GetValue().length()) {
				this->m_curPart.fields[std::get<1>(f)->GetValue().ToStdString()] = std::get<2>(f)->GetValue().ToStdString();
			}
		}
		if (this->mtgl_Rotate->GetValue()) this->m_curPart.rot = 1;
		else this->m_curPart.rot = 0;

		/* Creating resistor */
		this->m_drawPanel->drawPart(this->m_curPart);
		this->construct_part(buffer, this->m_curPart);
	} else if (!(fmap = this->getCnp(fvec, positions)).empty()) {
		this->m_curPart = cap_npDescriptor;

		this->m_curPart.vfields = fmap;
		for (const auto & f : this->m_cnpVFtxt) f.second->SetValue(fmap.at(f.first));
		for (const auto & f : this->m_resVFtxt) f.second->SetValue("");
		this->makeValue();

		this->m_curPart.lib = this->getDISCRETE_OLB().Upper().ToStdString();
		this->m_curPart.fields["P/N"] = fmap["P/N"];
		this->m_curPart.fields["Part Name"] = fmap["Part Name"];
		this->m_curPart.fields["PCB Footprint"] = fmap["PCB Footprint"];
		for (const auto & f : this->m_userProperties) {
			if (std::get<0>(f)->GetValue() && std::get<1>(f)->GetValue().length()) {
				this->m_curPart.fields[std::get<1>(f)->GetValue().ToStdString()] = std::get<2>(f)->GetValue().ToStdString();
			}
		}
		if (this->mtgl_Rotate->GetValue()) this->m_curPart.rot = 1;
		else this->m_curPart.rot = 0;

		/* Creating capacitor_np */
		this->m_drawPanel->drawPart(this->m_curPart);
		this->construct_part(buffer, this->m_curPart);
	} else {
		wxTheClipboard->Close();
		return;
	}

	wxCustomDataObject *out_data = new wxCustomDataObject{this->m_orcadFormat};
	out_data->SetData(65535, buffer);
	wxTheClipboard->SetData(out_data);

	wxTheClipboard->Close();
}

void MyFrame::replaceClipboardPart(struct partDescriptor &part)
{
	if (!wxTheClipboard->IsSupported(this->m_orcadFormat)) {
		return;
	}

	if (!wxTheClipboard->Open()) return;

	this->m_drawPanel->drawPart(part);
	this->construct_part(buffer, part);

	wxCustomDataObject *out_data = new wxCustomDataObject{this->m_orcadFormat};
	out_data->SetData(65535, buffer);
	wxTheClipboard->SetData(out_data);

	wxTheClipboard->Close();
}

void MyFrame::construct_part(void *buf, const struct partDescriptor &part)
{
	if (!buf) return;
	memset(buf, 0, sizeof(65535));
	unsigned char *o = (unsigned char *)buf;
	for (const auto &e : part.entries) {
		switch (e.t) {
			case eCONST:
				memcpy(o, e.d, e.s);
				o += e.s;
				break;
			case eLIB_PATH:
				o = write_string(o, part.lib);
				break;
			case eROTATION:
				*o++ = part.rot & 0xFF;
				break;
			case eREF_DESCR:
				o = part.REF.writeDescriptor(o);
				break;
			case eVAL_DESCR:
				o = part.VAL.writeDescriptor(o);
				break;
			case eREF_VALUE:
				o = write_string(o, part.REF.value);
				break;
			case eVAL_VALUE:
				o = write_string(o, part.VAL.value);
				break;
			case eFIELDS:
				{
					size_t sz = part.fields.size();
					*o++ = (sz >> 0) & 0xFF;
					*o++ = (sz >> 8) & 0xFF;
					for (const auto & f : part.fields) {
						o = write_string(o, f.first);
						o = write_string(o, f.second);
					}
				}
				break;
			case eNONE:
			default:
				return;
		}
	}
}

void MyFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
    // true is to force the frame to close
    Close(true);
}

void MyFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{
    wxMessageBox("CLERP to Schematic (OrCAD), version 0.04 alpha", "About CLERP2Schematic", wxOK|wxICON_INFORMATION, this);
}

ConfigFrame::ConfigFrame(wxWindow * parent)
	: wxFrame(parent, wxID_ANY, "Configrations")
{
	auto ts = new wxBoxSizer(wxVERTICAL);
	ts->Add((this->mp_configPanel = new ConfigPanel(this)), 1, wxEXPAND, 0);
	//ts->SetMinSize(600, 400);
	ts->SetSizeHints(this);
	//this->Layout();

	this->Bind(wxEVT_CLOSE_WINDOW,
			   [=](wxCloseEvent& e) {
				   if (e.CanVeto()) {
					   this->Show(0);
				   } else this->Destroy();
			   }, wxID_ANY);
}

ConfigPanel::ConfigPanel(wxWindow *parent, int id)
	: wxPanel(parent, id)
{
	wxConfigBase *cfg = wxConfigBase::Get();

	this->SetSizer(new wxBoxSizer(wxVERTICAL));

	{
		wxBoxSizer *rsz = new wxBoxSizer(wxHORIZONTAL);
		rsz->Add(new wxStaticText(this, wxID_ANY, "Components library"), 0, wxALIGN_CENTER_VERTICAL | wxRIGHT, 4);
		rsz->Add(new wxFilePickerCtrl(this, idFln_CompLib,
					cfg->Read("/components/lib", "Y:\\CLERP2ORCAD.OLB"),
					"Select library", "OrCAD library files (*.OLB)|*.OLB",
					wxDefaultPosition, wxSize(200, -1), wxFLP_USE_TEXTCTRL | wxFLP_OPEN), 1);
		this->GetSizer()->Add(rsz, 0, wxEXPAND | wxALL, 4);
	}
	{
		wxBoxSizer *rsz = new wxBoxSizer(wxHORIZONTAL);
		rsz->Add(new wxStaticText(this, wxID_ANY, "Value field separator"), 0, wxALIGN_CENTER_VERTICAL | wxRIGHT, 4);
		rsz->Add(new wxTextCtrl(this, idTxt_Separator,
					cfg->Read("/components/value-separator", ","),
					wxDefaultPosition, wxSize(200, -1), wxTE_PROCESS_ENTER), 1);
		this->GetSizer()->Add(rsz, 0, wxEXPAND | wxALL, 4);
	}

	Bind(wxEVT_FILEPICKER_CHANGED,
		 [this](wxFileDirPickerEvent& e) {
			 wxConfigBase *cfg = wxConfigBase::Get();
			 cfg->Write("/components/lib", e.GetPath());

		 }, idFln_CompLib);

	Bind(wxEVT_TEXT_ENTER,
		 [this](wxCommandEvent& e) {
			 wxTextCtrl* t = ((wxTextCtrl*)e.GetEventObject());
			 wxConfigBase *cfg = wxConfigBase::Get();
			 wxString v = t->GetValue();
			 if (!v.empty()) {
				 cfg->Write("/components/value-separator", v);
			 } else {
				 t->ChangeValue(cfg->Read("/components/value-separator", ","));
			 }
		 }, idTxt_Separator);

	Bind(wxEVT_TEXT,
		 [this](wxCommandEvent& e) {
			 wxTextCtrl* t = ((wxTextCtrl*)e.GetEventObject());
			 wxConfigBase *cfg = wxConfigBase::Get();
			 wxString v = t->GetValue();
			 if (!v.empty()) {
				 cfg->Write("/components/value-separator", v);
			 }
		 }, idTxt_Separator);
}


/* vim: set ts=4 sw=4 noet nowrap cin cino=j1,(0,ws,Ws ai: */
